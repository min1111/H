# Hyper link Http 서버 엔진 

하이퍼 링크 웹 엔진 템플릿 입니다. 

## Getting Started 

Spring boot 기반의  RESTful API 서버입니다. 

- Spring Version 
    - Spring boot Version 2.1.4 RELEASE
    - JPA, MariaDB, Lombok, security, aop 적용
- JDK Version : OpenJDK 11 
- Gradle 포함

- Gradle 사전 설치 
- JDK 11 사전설치
- Intellij IDE (될수 있으면 이클립스가 아닌 해당 IDE로 개발)

### 설정
classes/application.yml 

```
spring.datasource: 
  initialize: false
  url: jdbc:mysql://rxgp1.iptime.org:3380/test?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Seoul ## DB 도메인 for JPA
  jdbcUrl: jdbc:mysql://rxgp1.iptime.org:3380/test?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Seoul ## DB 도메인 for Mybatis
  driverClassName: com.mysql.cj.jdbc.Driver ## DB 드라이버명 (현재 MySQL or MariaDB)
  username: root							## 아이디	
  password: tla0420!@						## 비밀번호
  validationQuery: select 1
  testWhileIdle: true
  timeBetweenEvictionRunsMillis: 60000
  maxOpenPreparedStatements: 50
  poolPreparedStatements: true
  maxWait: 3000
  minIdle: 5
  maxActive: 20
  initialSize: 5

server.admin:
  front_url: http://localhost:4200	 # 관리자 URL  
  file_path: D:\\Workspaces\\work_trip\\mobi_admin_1.0.0\\src\\main\\webapp # 이미지 경로(서버 이중화를 위한 처리)


server.servlet:
  context-path: /server ## 서버 Context
  port: 8080        	## 서버 포트
  
```  

classes/logback.xml

```
<property name="logging_file"   value="/home/rxgp1/works/project/hyperlink/logs/" /> 
<!-- 상위 경로 로그 쌓이는 곳으로 설정 --/>
```



### Project Tree 

```
- java
├── kr.hyperlink.server.engine
│   ├── admin                         관리자 처리 Controller, Service, Repo
│   ├── api                           api 유저 처리 Controller, Service, Repo
│   ├── aspect                        Aspect 처리 클래스
│   ├── common                        Based Domain 
│   ├── config                        Spring boot 설정 관련 파일 모음
│   ├── constants                     상수 모음
│   ├── exception                     글로벌 예외 처리
│   ├── mapper                        MyBatis 매퍼
│   ├── security                      Spring security 설정
│   ├── utility                       처리 유틸 모음
│   ├── EngineApplication.java        기동 클래스
│   └── EngineFixture.java            실행시 최초 실행되는 클래스

- resources
├── mapper                            Mybatis 클래스 SQL XML
├── nav                               관리자 네비게이션 처리 json
├── static                            정적 파일 위치
├── application.yml                   Spring 설정 파일
├── banner.txt                        배너 파일
├── logback.xml                       로그백 설정 파일
└── mybatis-datasource-config.xml     Mybatis 설정 파일


- webapp
├── WEB-INF                            Mybatis 클래스 SQL XML
└   └── jsp                            jsp 파일 모음

```

### 개발환경

해당 프로젝트 진입

```
dev     : gradle clean build
real    : gradle clean build -Pprofile={빌드 파라메터}

src/main/resources-{빌드 파라메터 매칭} (기본 dev)
```
