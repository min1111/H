/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.domain.AdminUser.RoleType;
import kr.hyperlink.server.engine.admin.domain.AdminUser.Status;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.repo 
 * AdminUserRepo.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   관리자 Repo
 * @description		:	
 * </pre>
 *
 */
@Repository
public interface AdminUserRepo  extends JpaRepository<AdminUser, Long> { 
	
	public AdminUser findByUsername(String userName);

	@Query(
			"SELECT 																																			"
		+	" 	admin 																																			"
		+ 	"FROM AdminUser admin 																															"
		+	"WHERE admin.roleType = :roleType	 																											"
		+	"AND 	((:status IS NULL AND (1=1) OR :status IS NOT NULL AND (admin.status = :status)))												"
		+	"AND ((:searchType = 'ALL' AND (1=1) 																											"
		+ 	"	OR 	(:searchType = 'USER_NAME' AND (admin.name LIKE CONCAT('%', :searchText , '%'))) 												"
		+ 	"	OR 	(:searchType = 'USER_ID' AND (admin.username LIKE CONCAT('%', :searchText , '%'))) 												"
		+ 	"	OR 	(:searchType = 'EMAIL' AND (admin.email LIKE CONCAT('%', :searchText , '%'))) 													"
		+ 	"	OR 	(:searchType = 'MOBILE_NUMBER' AND (admin.mobileNumber LIKE CONCAT('%', :searchText , '%'))) 									"
		+ 	"))																																					"
		+ 	"ORDER BY admin.createDate DESC, admin.updateDate DESC																						"
	)
	public Page<AdminUser> findByAdmin(@Param("status") Status statusEnum, 
										@Param("roleType") RoleType roleType, 
										@Param("searchType") String type, 
										@Param("searchText") String search, Pageable constructPageSpecificationWithoutOrder);

}
