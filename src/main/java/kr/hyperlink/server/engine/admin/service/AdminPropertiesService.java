/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.hyperlink.server.engine.admin.domain.AdminProperties;
import kr.hyperlink.server.engine.admin.domain.AdminProperties.UpdateType;
import kr.hyperlink.server.engine.admin.model.AdminApi;
import kr.hyperlink.server.engine.admin.repo.AdminPropertiesRepo;
import kr.hyperlink.server.engine.api.domain.ApiUserDevice;
import kr.hyperlink.server.engine.api.domain.ApiUserDevice.OsType;
import kr.hyperlink.server.engine.api.model.AppModel;
import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.exception.AdminControllerException;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import kr.hyperlink.server.engine.utility.DBUtility;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.service 
 * AdminPropertiesService.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   사이트 관리자 설정 처리 Service
 * @description		:	
 * </pre>
 *
 */
@Service
public class AdminPropertiesService {
	
	@Autowired
	private AdminPropertiesRepo dao;
	
	public UserApi<AppModel> findByOsType(String osType) throws ApiControllerException {
		UserApi<AppModel> rtn = new UserApi<AppModel>();
		List<AdminProperties> props = dao.findAll(); 
		if(props.size() == 0) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR);
		}
		
		AdminProperties prop = props.get(0);
		
		ApiUserDevice.OsType osTypeEnum = (OsType) DBUtility.convertToEnumByString(Arrays.asList(ApiUserDevice.OsType.values()), osType);
		String packageName = "";
		String versionInfo = "";
		UpdateType updateType = null;
		
		
		if (osTypeEnum == ApiUserDevice.OsType.IOS) {
			packageName = prop.getBundleName();
			versionInfo = prop.getIosVersion();
			updateType = prop.getUpdateTypeIOS();
		} else if (osTypeEnum == ApiUserDevice.OsType.ANDROID){
			packageName = prop.getPackageName();
			versionInfo = prop.getAndroidVersion();
			updateType = prop.getUpdateTypeAnd();
		} else {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		
		AppModel responseModel = new AppModel();
		responseModel.setPackageName(packageName);
		responseModel.setVersionInfo(versionInfo);
		responseModel.setUpdateType(updateType);
		rtn.addModel(responseModel);
		
		return rtn;
	}

	public AdminApi<AdminProperties> getAdminProperties()  throws Exception, AdminControllerException {
		AdminApi<AdminProperties> rtn = new AdminApi<AdminProperties>();
		AdminProperties propUtil = dao.findAll().get(0);
		rtn.addModel(propUtil);
		return rtn;
	}

}
