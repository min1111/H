/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import kr.hyperlink.server.engine.common.CommonDomain;
import kr.hyperlink.server.engine.constants.DomainColumnConstants;
import kr.hyperlink.server.engine.constants.DomainTableConstants;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.domain 
 * Adminproperties.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   
 * @description		:	
 * </pre>
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name=DomainTableConstants.TBL_ADMIN_PROPERTIES)
public class AdminProperties extends CommonDomain {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 3317856778592897486L;
	
	/**
	 * <pre>
	 * kr.hyperlink.mobi.server.admin.domain 
	 * TripAdminProperties.java
	 * @author 			:	Hyper Link 심현섭
	 * @date 			:	2018. 2. 10.
	 * @brief			:   앱 버전 변경시 업데이트 설정 처리
	 * @description		:	
	 * </pre>
	 *
	 */
	public enum UpdateType {
		SUGGESTED,			// 업데이트 추천 (강제성 없음) 
		FORCED				// 업데이트 강제 실행
	}
	
	/** <code>bundleName</code> : iOS 번들명 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_BUNDLE_NAME, length=50, nullable=false)
	private String bundleName;
	
	/** <code>iosVersion</code> : iOS 최종버전 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_IOS_VERSION, length=50, nullable=false)
	private String iosVersion;
	
	/** <code>packageName</code> : 안드로이드 패키지명 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_PACKAGE_NAME, length=50, nullable=false)
	private String packageName;
	
	/** <code>androidVersion</code> : 안드로이드 최종버전  */
	@Column(name=DomainColumnConstants.TBL_COLUMN_ANDROID_VERSION, length=50, nullable=false)
	private String androidVersion;
	
	/** <code>sessionExpiredTime</code> : 세션 로그인 만료시간 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_SESSION_EXPIRED_TIME, nullable=false)
	private int sessionExpiredTime = 120;
	
	/** <code>loginFailedCnt</code> : 로그인 실패 횟수 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LOGIN_FAILED_CNT, nullable=false)
	private int loginFailedCnt = 5;
	
	/** <code>updateType</code> : 업데이트 시 강제 처리 타입 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_VERSION_UPDATE_TYPE_ANDROID, nullable=false)
	private UpdateType updateTypeAnd;
	
	/** <code>updateType</code> : 업데이트 시 강제 처리 타입 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_VERSION_UPDATE_TYPE_IOS, nullable=false)
	private UpdateType updateTypeIOS;


	/**
	 * @brief bundleName를 전달한다.
	 *
	 * @return bundleName
	 */
	public String getBundleName() {
		return bundleName;
	}

	/**
	 * @brief bundleName을 설정한다. 
	 *
	 * @param bundleName bundleName 
	 */
	
	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	/**
	 * @brief iosVersion를 전달한다.
	 *
	 * @return iosVersion
	 */
	public String getIosVersion() {
		return iosVersion;
	}

	/**
	 * @brief iosVersion을 설정한다. 
	 *
	 * @param iosVersion iosVersion 
	 */
	
	public void setIosVersion(String iosVersion) {
		this.iosVersion = iosVersion;
	}

	/**
	 * @brief sessionExpiredTime를 전달한다.
	 *
	 * @return sessionExpiredTime
	 */
	public int getSessionExpiredTime() {
		return sessionExpiredTime;
	}

	/**
	 * @brief sessionExpiredTime을 설정한다. 
	 *
	 * @param sessionExpiredTime sessionExpiredTime 
	 */
	
	public void setSessionExpiredTime(int sessionExpiredTime) {
		this.sessionExpiredTime = sessionExpiredTime;
	}

	/**
	 * @brief loginFailedCnt를 전달한다.
	 *
	 * @return loginFailedCnt
	 */
	public int getLoginFailedCnt() {
		return loginFailedCnt;
	}

	/**
	 * @brief loginFailedCnt을 설정한다. 
	 *
	 * @param loginFailedCnt loginFailedCnt 
	 */
	
	public void setLoginFailedCnt(int loginFailedCnt) {
		this.loginFailedCnt = loginFailedCnt;
	}

	/**
	 * @brief packageName를 전달한다.
	 *
	 * @return packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @brief packageName을 설정한다. 
	 *
	 * @param packageName packageName 
	 */
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @brief androidVersion를 전달한다.
	 *
	 * @return androidVersion
	 */
	public String getAndroidVersion() {
		return androidVersion;
	}

	/**
	 * @brief androidVersion을 설정한다. 
	 *
	 * @param androidVersion androidVersion 
	 */
	
	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	/**
	 * @brief updateTypeAnd를 전달한다.
	 *
	 * @return updateTypeAnd
	 */
	public UpdateType getUpdateTypeAnd() {
		return updateTypeAnd;
	}

	/**
	 * @brief updateTypeAnd을 설정한다. 
	 *
	 * @param updateTypeAnd updateTypeAnd 
	 */
	
	public void setUpdateTypeAnd(UpdateType updateTypeAnd) {
		this.updateTypeAnd = updateTypeAnd;
	}

	/**
	 * @brief updateTypeIOS를 전달한다.
	 *
	 * @return updateTypeIOS
	 */
	public UpdateType getUpdateTypeIOS() {
		return updateTypeIOS;
	}

	/**
	 * @brief updateTypeIOS을 설정한다. 
	 *
	 * @param updateTypeIOS updateTypeIOS 
	 */
	
	public void setUpdateTypeIOS(UpdateType updateTypeIOS) {
		this.updateTypeIOS = updateTypeIOS;
	}

	

}
