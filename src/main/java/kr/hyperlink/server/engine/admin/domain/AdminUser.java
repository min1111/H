/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import kr.hyperlink.server.engine.common.CommonDomain;
import kr.hyperlink.server.engine.constants.DomainColumnConstants;
import kr.hyperlink.server.engine.constants.DomainTableConstants;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.domain 
 * AdminUser.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   관리자 도메인
 * @description		:	
 * </pre>
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name=DomainTableConstants.TBL_ADMIN_USER)
public class AdminUser  extends CommonDomain implements UserDetails {
	
	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 4799522405398745873L;

	public enum RoleType {
		ROLE_USER,				// 유저 권한
		ROLE_ADMIN,				// 관리자 권한 
		ROLE_SUPER_MANAGER		// 슈퍼 관리자 권한
 	}
	
	public enum Status {
		USE, 					// 사용함
		NOT_USE					// 사용안함
		
	}
	
	/** <code>name</code> : 관리자명 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_NAME, length=50, nullable=false)
	private String name;
	
	/** <code>password</code> : 관리자 비밀번호 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_PASSWORD, length=255, nullable=false)
	private String password;
	
	/** <code>username</code> : 관리자 아이디 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_USER_ID, length=50, unique=true, nullable=false)
	private String username;
	
	/** <code>loginFailedCnt</code> : 로그인 실패 횟수 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LOGIN_FAILED_CNT, nullable=false)
	private int loginFailedCnt = 0;
	
	/** <code>isLock</code> : 관리자 잠금여부  */
	@Column(name=DomainColumnConstants.TBL_COLUMN_IS_LOCK, nullable=false)
	private boolean isLock = false;
	
	/** <code>roleType</code> : 관리자 권한  */
	@Column(name=DomainColumnConstants.TBL_COLUMN_ROLE_TYPE, nullable=false)
	private RoleType roleType;

	/** <code>status</code> :  관리자 상태 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_STATUS, nullable=false)
	private Status status = Status.USE;
	
	/** <code>email</code> : 관리자 이메일 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_EMAIL, length=100)
	private String email;
	
	/** <code>mobileNumber</code> : 관리자 전화번호  */
	@Column(name=DomainColumnConstants.TBL_COLUMN_MOBILE_NUMBER, length=100)
	private String mobileNumber;
	
	/** <code>sessionIntervalTime</code> : 세션 유효 시간 */
	@Transient
	private int sessionIntervalTime;
	
	@Transient
	private String nav;
	
	@Transient
	private String passwordConfirm;
	
	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		RoleType[] roles = RoleType.values();
		int limitRoleType = roleType.ordinal();
		for (int i = 0, l = roles.length; i < l; ++i) {
			RoleType role = roles[i];
			if (i <= limitRoleType) {
				GrantedAuthority auth = new SimpleGrantedAuthority(role.name());
				list.add(auth);
			}
		}
		return list;
	}
	
	
	

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}
	

	
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getUsername() {
		return username;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLoginFailedCnt() {
		return loginFailedCnt;
	}

	public void setLoginFailedCnt(int loginFailedCnt) {
		this.loginFailedCnt = loginFailedCnt;
	}

	public boolean isLock() {
		return isLock;
	}

	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the nav
	 */
	public String getNav() {
		return nav;
	}


	/**
	 * @param nav the nav to set
	 */
	public void setNav(String nav) {
		this.nav = nav;
	}
	
	
	/**
	 * @return the passwordConfirm
	 */
	public String getPasswordConfirm() {
		return passwordConfirm;
	}




	/**
	 * @param passwordConfirm the passwordConfirm to set
	 */
	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}




	/**
	 * check if user has a role, it is determined by {@link RoleType}'s ordinal
	 * @param role
	 * @return true if given role's ordinal is same or less than role's ordinal of user.
	 */
	public boolean hasRole(RoleType role) {
		if (getRoleType().ordinal() >= role.ordinal()) {
			return true;
		} else {
			return false;
		}
	}
	
	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}

	public int getSessionIntervalTime() {
		return sessionIntervalTime;
	}

	public void setSessionIntervalTime(int sessionIntervalTime) {
		this.sessionIntervalTime = sessionIntervalTime;
	}

}
