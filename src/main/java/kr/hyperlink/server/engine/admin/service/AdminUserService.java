/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.model.AdminApi;
import kr.hyperlink.server.engine.admin.repo.AdminUserRepo;
import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.exception.AdminControllerException;
import kr.hyperlink.server.engine.utility.DBUtility;
import kr.hyperlink.server.engine.utility.PageHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.service 
 * AdminUserService.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:  	관리자 서비스 클래스
 * @description		:	
 * </pre>
 *
 */
@Service
@Slf4j
public class AdminUserService implements UserDetailsService {
	

	private AdminUserRepo adminRepo;
	
	private PasswordEncoder encode;
	
	public AdminUserService(AdminUserRepo adminRepo, PasswordEncoder encode) {
		this.adminRepo = adminRepo;
		this.encode = encode;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final AdminUser user = adminRepo.findByUsername(username);
		
		if (user == null) {
			log.debug("[user is not found]");
			throw new UsernameNotFoundException(username + " is not found");	
		}
		
		return user;
	}
	
	public AdminUser findByUsername(String userName) {
		AdminUser user = adminRepo.findByUsername(userName);
		return user;
	}

	@Transactional(rollbackOn = {Exception.class, AdminControllerException.class})
	public AdminUser updateUser(String userName)  throws IOException {
		AdminUser user = adminRepo.findByUsername(userName);
		if (user == null) {
			throw new UsernameNotFoundException(userName + " is not found");	
		}
		user.setLock(false);
		user.setLoginFailedCnt(0);
		user = adminRepo.save(user);
		return user;
	}

	public AdminApi<AdminUser> getUserManagement(int page, int max, String status, String type, String search, String roleType)  throws AdminControllerException, Exception {
		AdminUser.Status statusEnum = (AdminUser.Status) DBUtility.convertToEnumByString(Arrays.asList(AdminUser.Status.values()), status);
		AdminUser.RoleType roleSuperManager = (AdminUser.RoleType) DBUtility.convertToEnumByString(Arrays.asList(AdminUser.RoleType.values()), roleType);
		Page<AdminUser> list = adminRepo.findByAdmin(statusEnum, roleSuperManager, type, search, PageHelper.constructPageSpecificationWithoutOrder(page, max));
		return new AdminApi<>(list);
	}

	@Transactional(rollbackOn = {Exception.class, AdminControllerException.class})
	public AdminApi<?> createAdmin(AdminUser adminUser) throws Exception, AdminControllerException {
		AdminApi<AdminUser> api = new AdminApi<>();
		
		AdminUser confirmUser = adminRepo.findByUsername(adminUser.getUsername());
		if(confirmUser != null) {
			api.setResStatus(AdminApi.RES_STATUS_CODE_FOR_SUCCESSFUL_WITH_NONE_CONTENTS);
			return api;
		}
		
		String password = encode.encode(adminUser.getPasswordConfirm());
		adminUser.setPassword(password);
		
		adminUser = adminRepo.save(adminUser);
		return new AdminApi<>();
	}

	public AdminApi<AdminUser> getAdminUser(long id)  throws Exception, AdminControllerException {
		Optional<AdminUser> updateUserIfNull = adminRepo.findById(id);
		if(updateUserIfNull.isEmpty()) {
			throw new AdminControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		AdminUser updateUser = updateUserIfNull.get();
		AdminApi<AdminUser> api = new AdminApi<>();
		api.addModel(updateUser);
		return api;
	}

	@Transactional(rollbackOn = {Exception.class, AdminControllerException.class})
	public AdminApi<?> updateAdmin(long id, AdminUser adminUser) throws Exception, AdminControllerException {
		Optional<AdminUser> adminUserIfNull = adminRepo.findById(id); 
		if(adminUserIfNull.isEmpty()) {
			throw new AdminControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		AdminUser updateUser = adminUserIfNull.get();
		updateUser.setEmail(adminUser.getEmail());
		updateUser.setMobileNumber(adminUser.getMobileNumber());
		updateUser.setName(adminUser.getName());
		if(adminUser.getPasswordConfirm() != null) {
			if(!adminUser.getPasswordConfirm().isEmpty()) {
				updateUser.setPassword(encode.encode(adminUser.getPasswordConfirm()));
			}
		}
		updateUser.setStatus(adminUser.getStatus());
		updateUser = adminRepo.save(updateUser);
		if(updateUser == null) {
			throw new AdminControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED);
		}
		
		return new AdminApi<>();
	}
	
	@Transactional(rollbackOn = {Exception.class, AdminControllerException.class})
	public AdminApi<?> deleteAdmin(long id) throws Exception, AdminControllerException {
		Optional<AdminUser> adminUserIfNull = adminRepo.findById(id); 
		if(adminUserIfNull.isEmpty()) {
			throw new AdminControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		adminRepo.delete(adminUserIfNull.get());
		return new AdminApi<>();
	}

}
