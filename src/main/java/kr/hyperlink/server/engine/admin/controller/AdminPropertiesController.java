/*
 * engine
 * Copyright 2019. 4. 18. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.hyperlink.server.engine.admin.domain.AdminProperties;
import kr.hyperlink.server.engine.admin.model.AdminApi;
import kr.hyperlink.server.engine.admin.service.AdminPropertiesService;
import kr.hyperlink.server.engine.exception.AdminControllerException;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.controller 
 * AdminProperties.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 18.
 * @brief			:   사이트 관리 Cointroller
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@RestController
@RequestMapping(value = "/admin/properties")
public class AdminPropertiesController {
	
	@Autowired
	private AdminPropertiesService service; 
	
	@RequestMapping(method = RequestMethod.GET)
	public AdminApi<AdminProperties> read () throws AdminControllerException {
		AdminApi<AdminProperties> rtn = null;
		try {
			rtn = service.getAdminProperties();
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}

}
