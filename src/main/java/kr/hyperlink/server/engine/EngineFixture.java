/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import kr.hyperlink.server.engine.admin.domain.AdminProperties;
import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.repo.AdminPropertiesRepo;
import kr.hyperlink.server.engine.admin.repo.AdminUserRepo;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine 
 * EngineFixture.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   초기 데이터 입력 클래스
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@Component
public class EngineFixture {
	
	public static final String DEFAULT_ADMIN_NAME = "admin";
	
	public static final String DEFAULT_ADMIN_PASSWORD = "qqqq1111!";
	
	public static final String DEFAULT_IOS_BUNDLE_NAME = "kr.hyperlink.app.sample";
	
	public static final String DEFAULT_IOS_VERSION = "1.0.0";
	
	public static final String DEFAULT_ANDROID_PACKAGE_NAME = "kr.hyperlink.app.sample";
	
	public static final String DEFAULT_ANDROID_VERSION = "1.0.0";
	
	private AdminUserRepo adminUserRepo;
	
	private AdminPropertiesRepo propertiesRepo;
	
	private PasswordEncoder encoder; 
	
	public EngineFixture(AdminUserRepo adminUserRepo, 
							AdminPropertiesRepo propertiesRepo, 
							PasswordEncoder encoder) {
		this.adminUserRepo = adminUserRepo;
		this.propertiesRepo = propertiesRepo;
		this.encoder = encoder;
	}
	
	@PostConstruct
	public void initailise() throws RuntimeException {
		log.info("Begin Fixture#initialise");
		
		try {
			
			AdminUser defaultUser = createDefaultUser(DEFAULT_ADMIN_NAME);
			if(defaultUser != null) {
				log.debug("[The Default User was created by initailise() ]");
				log.debug("[Default UserId : {}]", defaultUser.getUsername());
				log.debug("[Default UserName : {}]", defaultUser.getName());
				log.debug("[Default UserRole : {}]", defaultUser.getRoleType().toString());
			}
			
			AdminProperties property = new AdminProperties();
			List<AdminProperties> properties = propertiesRepo.findAll();
			
			boolean existsProperties = (propertiesRepo.findAll().size() == 0)?false:true;
			
			if(!existsProperties) {
				property.setBundleName(DEFAULT_IOS_BUNDLE_NAME);
				property.setIosVersion(DEFAULT_IOS_VERSION);
				property.setPackageName(DEFAULT_ANDROID_PACKAGE_NAME);
				property.setAndroidVersion(DEFAULT_ANDROID_VERSION);
				property.setUpdateTypeAnd(AdminProperties.UpdateType.SUGGESTED);
				property.setUpdateTypeIOS(AdminProperties.UpdateType.SUGGESTED);
				propertiesRepo.save(property);
			} else {
				property = properties.get(0);
			}
			
		} catch (Exception e) {
			log.error("[Trip Alert App was not init by error message : {} ]", e);
		}
		
		log.info("End Fixture#initialise");
		
	}
	
	private AdminUser createDefaultUser(String defaultUserId) throws Exception {
		AdminUser user = null;
		AdminUser findUser = adminUserRepo.findByUsername(defaultUserId);
		if(findUser == null) {
			user = new AdminUser();
			user.setName(defaultUserId);
			user.setUsername(defaultUserId);
			user.setRoleType(AdminUser.RoleType.ROLE_SUPER_MANAGER);
			log.debug("[password str : [{}] ", encoder.encode(DEFAULT_ADMIN_PASSWORD));
			user.setPassword(encoder.encode(DEFAULT_ADMIN_PASSWORD));
			user.setStatus(AdminUser.Status.USE);
			user.setEmail("hyperlink.lab@gmail.com");
			user = adminUserRepo.save(user);
		} 
		return user;
	}
}
