/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.constants;

import java.io.Serializable;

/**
 * <pre>
 * kr.hyperlink.server.engine.constants 
 * DomainColumnConstants.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   도메인 컬럼 상수 모음
 * @description		:	
 * </pre>
 *
 */

public class DomainColumnConstants implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 6105287872435183985L;
	
	
	/** <code>TBL_COLUMN_NAME</code> : 이름 */
	public static final String TBL_COLUMN_NAME = "NAME";
	
	
	/** <code>TBL_COLUMN_USER_ID</code> : 유저 아이디 */
	public static final String TBL_COLUMN_USER_ID = "USER_ID";
	
	/** <code>TBL_COLUMN_EMAIL</code> : 이메일 */
	public static final String TBL_COLUMN_EMAIL = "EMAIL";
	
	/** <code>TBL_COLUMN_TYPE</code> : 권한타입 */
	public static final String TBL_COLUMN_ROLE_TYPE = "ROLE_TYPE";
	
	/** <code>TBL_COLUMN_STATUS</code> : 상태값 */
	public static final String TBL_COLUMN_STATUS = "STATUS";
	
	/** <code>TBL_COLUMN_PASSWORD</code> : 비밀번호 */
	public static final String TBL_COLUMN_PASSWORD = "PASSWORD";
	
	/** <code>TBL_COLUMN_SESSION_EXPIRED_TIME</code> : 세션 만료일 */
	public static final String TBL_COLUMN_SESSION_EXPIRED_TIME = "SESSION_EXPIRED_TIME";
	
	/** <code>TBL_COLUMN_LOGIN_FAILED_CNT</code> : 로그인 실패 횟수 */
	public static final String TBL_COLUMN_LOGIN_FAILED_CNT = "LOGIN_FAILED_CNT";
	
	/** <code>TBL_COLUMN_IS_LOCK</code> : 관리자 잠금여부 */
	public static final String TBL_COLUMN_IS_LOCK = "IS_LOCK";
	
	/** <code>TBL_COLUMN_MOBILE_NUMBER</code> : 모바일 번호 */
	public static final String TBL_COLUMN_MOBILE_NUMBER = "MOBILE_NUMBER";
	
	/** <code>TBL_COLUMN_BUNDLE_NAME</code> : 번들명 */
	public static final String TBL_COLUMN_BUNDLE_NAME = "BUNDLE_NAME";
	
	/** <code>TBL_COLUMN_IOS_VERSION</code> : iOS 버전 */
	public static final String TBL_COLUMN_IOS_VERSION = "IOS_VERSION";
	
	/** <code>TBL_COLUMN_PACKAGE_NAME</code> : 퍄키지명 */
	public static final String TBL_COLUMN_PACKAGE_NAME = "PACKAGE_NAME";
	
	/** <code>TBL_COLUMN_ANDROID_VERSION</code> : 안드로이드 버전 */
	public static final String TBL_COLUMN_ANDROID_VERSION = "ANDROID_VERSION";
	
	/** <code>TBL_COLUMN_VERSION_UPDATE_TYPE</code> : 버전 업데이트 시 업데이트 타입  (안드로이드)*/
	public static final String TBL_COLUMN_VERSION_UPDATE_TYPE_ANDROID = "UPDATE_TYPE_ANDROID";
	
	/** <code>TBL_COLUMN_VERSION_UPDATE_TYPE_IOS</code> : 버전 업데이트 시 업데이트 타입 (iOS) */
	public static final String TBL_COLUMN_VERSION_UPDATE_TYPE_IOS = "UPDATE_TYPE_IOS";
	
	/** <code>TBL_COLUMN_USER_ID_FACEBOOK</code> : 유저 아이디 (페이스북) */
	public static final String TBL_COLUMN_USER_ID_FACEBOOK = "USER_ID_FACEBOOK";
	
	/** <code>TBL_COLUMN_USER_ID_NAVER</code> : 유저 아이디(네이버) */
	public static final String TBL_COLUMN_USER_ID_NAVER = "USER_ID_NAVER";
	
	/** <code>TBL_COLUMN_USER_ID_KAKAO</code> : 유저 아이디(카카오톡) */
	public static final String TBL_COLUMN_USER_ID_KAKAO = "USER_ID_KAKAO";
	
	/** <code>TBL_COLUMN_USER_ID_SNS</code> : 유저 아이디 SNS */
	public static final String TBL_COLUMN_USER_ID_SNS = "USER_ID_SNS";
	
	/** <code>TBL_COLUMN_EMAIL_FACEBOOK</code> : 페이스북 이메일 */
	public static final String TBL_COLUMN_EMAIL_FACEBOOK = "EMAIL_FACEBOOK";
	
	/** <code>TBL_COLUMN_EMAIL_NAVER</code> : 네이버 이메일 */
	public static final String TBL_COLUMN_EMAIL_NAVER = "EMAIL_NAVER";
	
	/** <code>TBL_COLUMN_EMAIL_KAKAO</code> : 카카오 이메일 */
	public static final String TBL_COLUMN_EMAIL_KAKAO = "EMAIL_KAKAO";
	
	/** <code>TBL_COLUMN_EMAIL_SNS</code> : SNS 이메일 */
	public static final String TBL_COLUMN_EMAIL_SNS = "EMAIL_SNS";
	
	/** <code>TBL_COLUMN_PROFILE_IMAGE_PATH</code> : 프로필 이미지 경로 */
	public static final String TBL_COLUMN_PROFILE_IMAGE_PATH = "PROFILE_IMAGE_PATH";
	
	/** <code>TBL_COLUMN_LATEST_LOGIN</code> : 마지막 로그인 시간 */
	public static final String TBL_COLUMN_LATEST_LOGIN = "LATEST_LOGIN";
	
	/** <code>TBL_COLUMN_CHECKOUT_TIME</code> : 회원탈퇴 */
	public static final String TBL_COLUMN_CHECKOUT_TIME = "CHECKOUT_TIME";
	
	/** <code>TBL_COLUMN_LOGIN_TYPE</code> : 유저 로그인 타입 */
	public static final String TBL_COLUMN_LOGIN_TYPE = "LOGIN_TYPE";
	
	/** <code>TBL_COLUMN_LOGOUT_TIME</code> : 로그아웃 시간 */
	public static final String TBL_COLUMN_LOGOUT_TIME = "LOGOUT_TIME";
	
	/** <code>TBL_COLUMN_AUTH_TOKEN</code> : 인증 토큰 */
	public static final String TBL_COLUMN_AUTH_TOKEN = "AUTH_TOKEN";
	
	/** <code>TBL_COLUMN_AUTH_TOKEN_SNS</code> : 인증토큰 SNS */
	public static final String TBL_COLUMN_AUTH_TOKEN_SNS = "AUTH_TOKEN_SNS";
	
	/** <code>TBL_COLUMN_DEVICE_ID</code> : 장치 ID(고유값) */
	public static final String TBL_COLUMN_DEVICE_ID = "DEVICE_ID";
	
	/** <code>TBL_COLUMN_PUSH_ID</code> : 푸시 아이디 */
	public static final String TBL_COLUMN_PUSH_ID = "PUSH_ID";
	
	/** <code>TBL_COLUMN_LOGIN_EXPIRED_DATE</code> : 장치 로그인 인증 만료 시간 */
	public static final String TBL_COLUMN_LOGIN_EXPIRED_DATE = "LOGIN_EXPIRED_DATE";
	
	/** <code>TBL_COLUMN_MODEL_NAME</code> : 장치 모델명 */
	public static final String TBL_COLUMN_MODEL_NAME = "MODEL_NAME";
	
	/** <code>TBL_COLUMN_OS_TYPE</code> : 장치 OS 타입 */
	public static final String TBL_COLUMN_OS_TYPE = "OS_TYPE";
	
	/** <code>TBL_COLUMN_APP_VERSION</code> : 앱 버전 */
	public static final String TBL_COLUMN_APP_VERSION = "APP_VERSION";


}
