/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.constants;

import java.io.Serializable;

/**
 * <pre>
 * kr.hyperlink.server.engine.constants 
 * CommonConstants.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   공통 상수 모음
 * @description		:	
 * </pre>
 *
 */

public class CommonConstants  implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 3390582675151605042L;
	
	
	/** <code>SETTING_ORDERD_1</code> : 스프링 시큐리티 정렬기준 */
	public static final int SETTING_ORDERD_1 = 1; 
	
	
	/** <code>SETTING_ORDERD_2</code> : 스프링 시큐리티 정렬기준 */
	public static final int SETTING_ORDERD_2 = 2;
	
	
	/** <code>SETTING_ORDERD_3</code> : 스프링 시큐리티 정렬기준  */
	public static final int SETTING_ORDERD_3 = 3;

}
