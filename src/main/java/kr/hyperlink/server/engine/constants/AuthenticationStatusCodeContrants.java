/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.constants;

import java.io.Serializable;

/**
 * <pre>
 * kr.hyperlink.server.engine.constants 
 * AuthenticationStatusCodeContrants.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   인증 처리 상수 모음
 * @description		:	
 * </pre>
 *
 */

public class AuthenticationStatusCodeContrants  implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -3831571020204650046L;
	
	// 요청성공
	public static final int OK = 200; 
	
	// 인증 정보 없음
	public static final int UNAUTHENTICATION = 400; 
	
	// 로그인 실패
	public static final int AUTHENTICATION_FAILURE = 410; 
	
	// 접근 권한 없음
	public static final int ACCESS_FAILURE = 440; 
	
	//유저 사용 불가
	public static final int AUTHENTICATION_FAILED_FOR_USER_NOT_USED = 406;
	
	//유저 잠금 상태
	public static final int AUTHENTICATION_FAILED_FOR_USER_IS_LOCK = 402;
	
	//유저 사용 허용 불가
	public static final int AUTHENTICATION_FAILED_FOR_USER_IS_NOT_ALLOWS = 405;
	
	// 로그인 실패 (비밀번호 실패로 인한 유저 잠김)
	public static final int AUTHENTICATION_FAILED_FOR_LOCKED = 407; 
	
	//유저 셀프 페이지 리다이렉션
	public static final int AUTHENTICATION_SUCCESSFUL_TO_REDIRECT_TO_SELF_PAGE = 201;
	
	//유저 최초 비밀번호 설정 리다이렉션
	public static final int AUTHENTICATION_SUCCESSFUL_TO_REDIRECT_TO_INIT_PASSWORD = 203;
	
	//유저 비밀번호 변경 처리 리다이렉션
	public static final int AUTHENTICATION_SUCCESSFUL_TO_REDIRECT_TO_CHANGE_PASSWORD = 204;

}
