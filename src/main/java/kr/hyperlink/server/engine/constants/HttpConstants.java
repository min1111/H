/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.constants;

import java.io.Serializable;

/**
 * <pre>
 * kr.hyperlink.server.engine.constants 
 * HttpConstants.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   HTTP 전송 상수
 * @description		:	
 * </pre>
 *
 */

public class HttpConstants implements Serializable {


	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 7053287699523433986L;

	/** <code>FACEBOOK_API_URL</code> : 페이스북  graph api url */
	public static final String FACEBOOK_API_URL = "https://graph.facebook.com";
	
	/** <code>FACEBOOK_API_ME_URL</code> : 페이스북  me API */
	public static final String FACEBOOK_API_ME_URL = FACEBOOK_API_URL+"/me";
	
	/** <code>FACEBOOK_PARAMS_ACCESS_TOKEN</code> : 페이스북 access token parameter key */
	public static final String FACEBOOK_PARAMS_ACCESS_TOKEN = "access_token"; 
	
	
	/** <code>NAVER_API_URL</code> : 네이버 로그인 검증 URL */
	public static final String NAVER_API_URL = "https://openapi.naver.com/v1/nid/me";
	
	/** <code>KAKAO_API_URL</code> : 카카오 로그인 검증 URL */
	public static final String KAKAO_API_URL = "https://kapi.kakao.com/v1/api/talk/profile";
	
	/** <code>SNS_PARAMS_BEARER</code> : SNS 로그인 HEADER Bearer 값 */
	public static final String SNS_PARAMS_BEARER = "Bearer "; 
	
	/** <code>EMPTY</code> : 공백값 */
	public static final String EMPTY = " ";
	
}