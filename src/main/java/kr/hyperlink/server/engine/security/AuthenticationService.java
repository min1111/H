/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.hyperlink.server.engine.utility.FileHandler;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.hyperlink.server.engine.admin.domain.AdminProperties;
import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.repo.AdminPropertiesRepo;
import kr.hyperlink.server.engine.admin.service.AdminUserService;
import kr.hyperlink.server.engine.constants.AuthenticationStatusCodeContrants;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * AuthenticationService.java
 * @author 			:	Hyper Link 심현섭
 * @date 				:	2019. 4. 11.
 * @brief				:   로그인 처리 서비스
 * @description		:	
 * </pre>
 *
 */
@Service
public class AuthenticationService {
	
	private AdminUserService userService;
	
	private AdminPropertiesRepo propsRepo;
	
	private FileHandler fileHandler;
	
	
	public AuthenticationService(AdminUserService userService, 
										AdminPropertiesRepo propsRepo, 
										FileHandler fileHandler) {
		this.userService = userService;
		this.propsRepo = propsRepo;
		this.fileHandler = fileHandler;
		
	}

	public void successfulLogin(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		
		int code = AuthenticationStatusCodeContrants.OK;
		String userName = request.getParameter("j_username");
		
		AdminUser user = userService.findByUsername(userName);
		if(user != null) {
			if(user.isLock()) {
				code = AuthenticationStatusCodeContrants.AUTHENTICATION_FAILED_FOR_LOCKED;
			}
			if(user.getStatus() == AdminUser.Status.NOT_USE) {
				code = AuthenticationStatusCodeContrants.AUTHENTICATION_FAILED_FOR_USER_NOT_USED;
			}
		} else {
			code = AuthenticationStatusCodeContrants.UNAUTHENTICATION;
		}
		
		response.setContentType("application/json;charset=UTF8");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		AuthenticationModel authenticationModel = new AuthenticationModel(); 
		
		switch (code) {
		case AuthenticationStatusCodeContrants.OK : {
			
			AdminUser admin = userService.updateUser(userName);
			
			AdminProperties props = propsRepo.findAll().get(0);
			
			int sessionExpiredTime = props.getSessionExpiredTime();
			
			HttpSession session = request.getSession();
			session.setMaxInactiveInterval(sessionExpiredTime);
			admin.setSessionIntervalTime(sessionExpiredTime);
			
			String fileName = admin.getRoleType().toString() + ".JSON";
			
			JSONObject nav = fileHandler.getNavFile(fileName);
			
			admin.setNav(nav.toString());
			
			Map<String, Object> userInfo = new HashMap<String, Object>();
			
			userInfo.put("userInfo", admin);
			
			authenticationModel.setError(true);
			authenticationModel.setMessage("Login is successful with userID");
			authenticationModel.setStatusCode(code);
			authenticationModel.setData(userInfo);
			
			break;

		}
		default : {
			request.getSession().invalidate();
			authentication.setAuthenticated(false);
			authenticationModel.setError(false);
			authenticationModel.setMessage("Login Failed");
			authenticationModel.setStatusCode(code);
			break;
		}
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(authenticationModel);
		PrintWriter out = response.getWriter();
		out.print(json);
		out.flush();
		out.close();
	}

	public void failedLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("application/json;charset=UTF8");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		int code = AuthenticationStatusCodeContrants.AUTHENTICATION_FAILURE;
		
		AuthenticationModel authenticationModel = new AuthenticationModel();
		
		switch (code) {
		case AuthenticationStatusCodeContrants.AUTHENTICATION_FAILURE : {
			// String userName = request.getParameter("j_username");
			
			// TODO: 로그인 실패 관련 처리 
			
			authenticationModel.setError(false);
			authenticationModel.setMessage("Login Failed");
			authenticationModel.setStatusCode(code);
			break;
		}	
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(authenticationModel);
		PrintWriter out = response.getWriter();
		out.print(json);
		out.flush();
		out.close();
	}

	public void successLogout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		response.setContentType("APPLICATION/JSON");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);

		int code = AuthenticationStatusCodeContrants.OK;
		
		AuthenticationModel authenticationModel = new AuthenticationModel();
		switch (code) {
		case AuthenticationStatusCodeContrants.OK : {
			authenticationModel.setError(true);
			authenticationModel.setMessage("Logout is successful");
			authenticationModel.setStatusCode(code);
			break;
		}	
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(authenticationModel);
		PrintWriter out = response.getWriter();
		out.print(json);
		out.flush();
		out.close();
	}
	
	
	
}
