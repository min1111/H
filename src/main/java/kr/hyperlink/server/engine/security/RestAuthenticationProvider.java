/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import kr.hyperlink.server.engine.api.domain.ApiUser;
import kr.hyperlink.server.engine.api.service.ApiUserService;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * RestAuthenticationProvider.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   인증 토큰 유효 여부 처리
 * @description		:	
 * </pre>
 *
 */
@Component
public class RestAuthenticationProvider implements AuthenticationProvider {

	
	//This would be a JPA repository to snag your user entities
    private final ApiUserService userService;
    
    @Autowired
    public RestAuthenticationProvider(ApiUserService userService) {
        this.userService = userService;
    }    
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    	RestAuthenticationToken demoAuthentication = (RestAuthenticationToken) authentication; 
    	String deviceToken = demoAuthentication.getAuthToken();
        
    	ApiUser user = userService.findByAuthToken(deviceToken);
		if(user == null) {
			throw new BadCredentialsException("authToken was not found ..");
		}
		
		if(user.getStatus() == ApiUser.UserStatus.RESET_PASSWD) {
			throw new BadCredentialsException("Reset password by user ..");
		}
		
		Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, null, authorities);
		return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return RestAuthenticationToken.class.isAssignableFrom(authentication);
    }

	
}
