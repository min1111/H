/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.service.AdminUserService;
import kr.hyperlink.server.engine.api.domain.ApiUser;
import kr.hyperlink.server.engine.api.service.ApiUserService;
import kr.hyperlink.server.engine.constants.CommonConstants;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * ServerSecurityConfigration.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   Spring security 설정
 * @description		:	
 * </pre>
 *
 */
@Configuration
@EnableWebSecurity
public class ServerSecurityConfigration {
	
	@Configuration
    @Order(CommonConstants.SETTING_ORDERD_1)
    public static class AdminConfigurationAdapter extends WebSecurityConfigurerAdapter {

		@Autowired 
		private LoginSuccessHandler loginSuccessHandler;
		
		@Autowired
		private LoginFaiureHandler loginFailureHandler;
		
		@Autowired
		private LogoutSuccessHandler logoutSuccessHandler;
		
		@Autowired
	    private RestAuthenticationEntryPoint authenticationEntryPoint;
		
		@Autowired 
		private AdminUserService userService;

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			
			httpSecurity.csrf().disable();
			httpSecurity.cors();
			httpSecurity
				.exceptionHandling()
					.authenticationEntryPoint(authenticationEntryPoint)
				.and()
				.formLogin()
					.loginProcessingUrl("/admin/auth")
					.usernameParameter("j_username")
					.passwordParameter("j_password")
					.successHandler(loginSuccessHandler)
					.failureHandler(loginFailureHandler)
	            .and()
	            .logout()
	            	.logoutUrl("/admin/logout")
	            	.deleteCookies("JSESSIONID")
	            	.invalidateHttpSession(true)
					.logoutSuccessHandler(logoutSuccessHandler)
				.and()		
				.antMatcher("/admin/**")
		            .authorizeRequests()
		            .antMatchers("/admin/auth").permitAll()
		            .antMatchers("/admin/**").hasAnyAuthority(new String[]{AdminUser.RoleType.ROLE_ADMIN.toString(), AdminUser.RoleType.ROLE_SUPER_MANAGER.toString()})
		        .and()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
					.sessionFixation().migrateSession();
		}
		
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userService);
			// In case of password encryption - for production site
			auth.userDetailsService(userService).passwordEncoder(ServerSecurityConfigration.passwordEncoder());
		}

		@Value("${server.admin.front_url}")
		private String frontDomain;
		
		@Bean
		protected CorsConfigurationSource corsConfigurationSource() {
			CorsConfiguration configuration = new CorsConfiguration();
	        configuration.setAllowedOrigins(Arrays.asList(frontDomain));
			//configuration.setAllowedOrigins(Arrays.asList("*"));
	        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PATCH", "PUT", "DELETE"));
	        configuration.setAllowCredentials(true);
	        configuration.setAllowedHeaders(Arrays.asList("Content-Type", "Accept", "Origin", "Authorization", "X-Auth-Token", "Pragma", "Expires", "Cache-Control"));
	        configuration.setExposedHeaders(Arrays.asList("X-Auth-Token"));
	        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        source.registerCorsConfiguration("/admin/**", configuration);
	        return source;
	    }
    }

	
	@Configuration
    @Order(CommonConstants.SETTING_ORDERD_3)
    public static class ApiConfigurationAdapter extends WebSecurityConfigurerAdapter {
		
		@Autowired
	    private RestAuthenticationEntryPoint authenticationEntryPoint;
		
		@Autowired
		private RestAuthenticationProvider restAuthenticationProvider;
		
		@Autowired 
		private ApiUserService userService;
		
		@Override
        protected void configure(HttpSecurity http) throws Exception {
			
			http.csrf().disable();
			http.cors();
			
			http
			.exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint)
			.and()
			.antMatcher("/api/**")
					.authorizeRequests()
					.antMatchers("/api/images/**").permitAll()
					.antMatchers("/api/login/**").permitAll()
					.antMatchers("/api/app/**").permitAll()

		            .antMatchers("/api/**").hasAuthority(ApiUser.RoleType.ROLE_USER.toString())
	            .and()
		            .addFilterBefore(new AuthenticationTokenFilter(userService), BasicAuthenticationFilter.class)
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }
		
		@Override
	    public void configure(AuthenticationManagerBuilder auth) throws Exception {        
	        auth.authenticationProvider(restAuthenticationProvider);        
	        auth.userDetailsService(userService).passwordEncoder(ServerSecurityConfigration.passwordEncoder());
	    }   
	}
	
	@Bean
	public static PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

}
