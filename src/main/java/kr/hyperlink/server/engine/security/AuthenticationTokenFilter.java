/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.api.service.ApiUserService;
import kr.hyperlink.server.engine.exception.ApiControllerException;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * AuthenticationTokenFilter.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   인증 토큰 처리 필터
 * @description		:	
 * </pre>
 *
 */
@Component
public class AuthenticationTokenFilter  extends GenericFilterBean {

	
	@Autowired
	private ApiUserService userService;
	
	@Autowired
    public AuthenticationTokenFilter(ApiUserService userService) {
        this.userService = userService;
    }    
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = this.getAsHttpRequest(request); 
		HttpServletResponse httpResponse = this.getAsHttpResponse(response);
		
		String xAuthToken = extractAuthTokenFromRequest(httpRequest);
		boolean isSuccessfulLogin = true;
		String resStatus = UserApi.RES_STATUS_CODE_FOR_FAILED_IS_EXPIRED_USER_TOKEN;

		if(xAuthToken != null) {
			try {
				resStatus = userService.validateUserToken(xAuthToken);
			} catch (ApiControllerException e) {
				resStatus = e.getCode();
			} catch (Exception e) {
				e.printStackTrace();
				resStatus = UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR;
			}
			
			if(resStatus.equals(UserApi.RES_STATUS_CODE_FOR_SUCCESSFUL)) {
				isSuccessfulLogin = true;
				Authentication auth = new RestAuthenticationToken(xAuthToken);
				SecurityContextHolder.getContext().setAuthentication(auth);    
			} else {
				isSuccessfulLogin = false;
			}
		} 
		
		
		if(!isSuccessfulLogin) {
			httpResponse.setContentType("application/json;charset=UTF8");
			httpResponse.setCharacterEncoding("UTF-8");
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(new UserApi<>(resStatus));
			PrintWriter out = response.getWriter();
			out.print(json);
			out.flush();
			out.close();
		} else {
			chain.doFilter(request, response);
		}
		
	}
	
	
	
	private HttpServletRequest getAsHttpRequest(ServletRequest request) {
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}
		return (HttpServletRequest) request;
	}
	
	private HttpServletResponse getAsHttpResponse(ServletResponse request) {
		if (!(request instanceof HttpServletResponse)) {
			throw new RuntimeException("Expecting an HTTP request");
		}
		return (HttpServletResponse) request;
	}



	
	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
		String authToken = httpRequest.getHeader("X-Auth-Token");
		return authToken;
	}

}
