/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import kr.hyperlink.server.engine.api.domain.ApiUser;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * RestAuthenticationToken.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   인증 토큰 모델
 * @description		:	
 * </pre>
 *
 */

public class RestAuthenticationToken extends AbstractAuthenticationToken {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -1436561009402701220L;

	private ApiUser authenticatedUser;

	private String authToken;

	public RestAuthenticationToken(String authToken) {
    	super(null);
    	this.authToken = authToken;
    	
    }
    
    public RestAuthenticationToken(Collection<? extends GrantedAuthority> authorities, ApiUser authenticatedUser, String authToken) {
        super(authorities);
        this.authToken = authToken;
        this.authenticatedUser = authenticatedUser;
    }

    @Override
    public Object getCredentials() {
        return authenticatedUser.getPassword();
    }

    @Override
    public Object getPrincipal() {
        return authenticatedUser;
    }

    public String getAuthToken() {
        return authToken;
    }

}
