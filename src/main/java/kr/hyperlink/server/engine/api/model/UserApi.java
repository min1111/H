/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import kr.hyperlink.server.engine.constants.AuthenticationStatusCodeContrants;

/**
 * <pre>
 * kr.hyperlink.server.engine.user.model 
 * UserApi.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저 API 반환 모델
 * @description		:	
 * </pre>
 *
 */

public class  UserApi<T> extends AuthenticationStatusCodeContrants implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -5408466694587083193L;
	
	public static final String KEY_FOR_PAGE = "page";
	
	public static final String KEY_FOR_MODEL = "model";
	
	public static final String KEY_FOR_LIST = "list";
	
	public static final String KEY_FOR_ID = "id";
	
	/** <code>RES_STATUS_CODE_FOR_SUCCESSFUL</code> : Response code : 성공 */
	public static final String RES_STATUS_CODE_FOR_SUCCESSFUL = "R200";
	
	/** <code>RES_STATUS_CODE_FOR_SUCCESSFUL_WITH_NONE_CONTENTS</code> : HTTP 는 성공이나 컨텐츠가 없음  */
	public static final String RES_STATUS_CODE_FOR_SUCCESSFUL_WITH_NONE_CONTENTS = "R204";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_LOGINED</code> : 로그인 실패 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_LOGINED = "R401";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_FACEBOOK_LOGIN</code> : SNS 로그인 검증 에러 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_SNS_LOGIN = "R403";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_EXPIRED_USER_TOKEN</code> : 유저 토큰 만료 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_EXPIRED_USER_TOKEN = "R407";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_USER_NOT_USE</code> : 유저 사용 불가 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_USER_NOT_USE = "R408";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_USER_DUPLICATED</code> : 유저 아이디 중복 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_USER_DUPLICATED = "R412";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_EMAIL_NOT_MACHING</code> : 유저명 & 이메일 없음 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_EMAIL_NOT_MACHING = "R413";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_NOT_EMAIL</code> : 이메일 존재하지 않음 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_NOT_EMAIL = "R414";
	
	/** <code>RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED</code> : Response Code: DB 입력/수정 오류 */
	public static final String RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED = "R511";
	
	/** <code>RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR</code> : Response Code: 서버에러  */
	public static final String RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR = "R500";
	
	/** <code>RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID</code> : Response Code: 입력 파라메터 에러 */
	public static final String RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID = "R503";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_USER_DUPLICATION_CHECK</code> : 회원가입 유저 중복 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_USER_DUPLICATION_CHECK = "R406";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_USER_EMAIL_DUPLICATION_CHECK</code> : 회원 가입 이메일 중복 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_USER_EMAIL_DUPLICATION_CHECK = "R407";
	
	/** <code>RES_STATUS_CODE_FOR_FAILED_IS_FACEBOOK_LOGIN</code> : 페이스북 로그인 검증 에러 */
	public static final String RES_STATUS_CODE_FOR_FAILED_IS_FACEBOOK_LOGIN = "R403";
	
	/** <code>status</code> : Http status */
	private int status;
	
	/** <code>message</code> : Http status message */
	private String message = "";
	
	/** <code>resStatus</code> : api status code */
	private String resStatus;
	
	/** <code>data</code> : api response data */
	private Map<String, Object> data;
	
	public UserApi() {
		data = new HashMap<String, Object>();
		this.status = OK;
		resStatus = RES_STATUS_CODE_FOR_SUCCESSFUL;
	}
	
	public UserApi(String resStatus) {
		data = new HashMap<String, Object>();
		this.status = OK;
		this.resStatus = resStatus;
	}
	
	public UserApi(Page<T> value) {
		data = new HashMap<String, Object>();
		this.status = OK;
		this.resStatus = RES_STATUS_CODE_FOR_SUCCESSFUL;
		this.addPage(value);
	}
	
	/**
	 * @brief response data 를 추가한다.
	 *
	 * @param key response data key
	 * @param value response data value
	 */
	public void addData(String key, Object value) {
		data.put(key, value);
	}
	
	/**
	 * @brief response data 의 page 데이터를 추가한다.
	 *
	 * @param value response page data
	 */
	public void addPage(Page<T> value) {
		data.put(KEY_FOR_PAGE, value);
	}
	
	/**
	 * @brief Response Data의 해당 객체를 추가한다.
	 *
	 * @param value 해당 Model 객체
	 */
	public void addModel(T value) {
		data.put(KEY_FOR_MODEL, value);
	}
	
	/**
	 * @brief  Response Data 해당 객체를 추가
	 *
	 * @param value
	 */
	public void addList(List<T> value) {
		data.put(KEY_FOR_LIST, value);
	}
	
	/**
	 * @brief Response Data 의 해당객체 아이디를 추가한다.
	 *
	 * @param id 해당 Model ID
	 */
	public void addId(long id) {
		data.put(KEY_FOR_ID, id);
	}
	
	/**
	 * @brief  response 데이터를 삭제한다. 
	 *
	 * @param key  삭제할 키
	 */
	public void removeData(String key) {
		data.remove(key);
	}
	
	/**
	 * @brief  페이지 데이터를 삭제한다.
	 *
	 */
	public void removePage() {
		data.remove(KEY_FOR_PAGE);
	}
	
	/**
	 * @brief  해당 모델 객체를 삭제한다.
	 *
	 */
	public void removeModel() {
		data.remove(KEY_FOR_MODEL);
	}
	
	/**
	 * @brief  해당 모델 객체의 ID 를 삭제한다.
	 *
	 */
	public void removeId() {
		data.remove(KEY_FOR_ID);
	}

	/**
	 * @brief data를 전달한다.
	 *
	 * @return data
	 */
	public Map<String, Object> getData() {
		return data;
	}

	/**
	 * @brief status를 전달한다.
	 *
	 * @return status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @brief status을 설정한다. 
	 *
	 * @param status status 
	 */
	
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @brief message를 전달한다.
	 *
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @brief message을 설정한다. 
	 *
	 * @param message message 
	 */
	
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @brief resStatus를 전달한다.
	 *
	 * @return resStatus
	 */
	public String getResStatus() {
		return resStatus;
	}

	/**
	 * @brief resStatus을 설정한다. 
	 *
	 * @param resStatus resStatus 
	 */
	
	public void setResStatus(String resStatus) {
		this.resStatus = resStatus;
	}

}
