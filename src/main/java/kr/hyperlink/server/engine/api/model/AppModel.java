/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.model;

import java.io.Serializable;

import kr.hyperlink.server.engine.admin.domain.AdminProperties.UpdateType;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.model 
 * AppModel.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   App Information 모델
 * @description		:	
 * </pre>
 *
 */

public class AppModel implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 5264374386297547205L;
	
	/** <code>versionInfo</code> : 현재 앱 버전  */
	private String versionInfo;
	
	/** <code>packageName</code> : 현재 앱 패키지(번들)명 */
	private String packageName;
	
	/** <code>updateType</code> : 앱 업데이트 타입 */
	private UpdateType updateType;
	
	/** <code>pushId</code> : 푸시 아이디 */
	private String pushId;

	/**
	 * @brief versionInfo를 전달한다.
	 *
	 * @return versionInfo
	 */
	public String getVersionInfo() {
		return versionInfo;
	}

	/**
	 * @brief versionInfo을 설정한다. 
	 *
	 * @param versionInfo versionInfo 
	 */
	
	public void setVersionInfo(String versionInfo) {
		this.versionInfo = versionInfo;
	}

	/**
	 * @brief packageName를 전달한다.
	 *
	 * @return packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @brief packageName을 설정한다. 
	 *
	 * @param packageName packageName 
	 */
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @brief updateType를 전달한다.
	 *
	 * @return updateType
	 */
	public UpdateType getUpdateType() {
		return updateType;
	}

	/**
	 * @brief updateType을 설정한다. 
	 *
	 * @param updateType updateType 
	 */
	
	public void setUpdateType(UpdateType updateType) {
		this.updateType = updateType;
	}

	/**
	 * @brief pushId를 전달한다.
	 *
	 * @return pushId
	 */
	public String getPushId() {
		return pushId;
	}

	/**
	 * @brief pushId을 설정한다. 
	 *
	 * @param pushId pushId 
	 */
	
	public void setPushId(String pushId) {
		this.pushId = pushId;
	}
	
	
	

}
