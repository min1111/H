/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import kr.hyperlink.server.engine.common.CommonDomain;
import kr.hyperlink.server.engine.constants.DomainColumnConstants;
import kr.hyperlink.server.engine.constants.DomainTableConstants;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.domain 
 * ApiUserDevice.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저 장치 도메인
 * @description		:	
 * </pre>
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name=DomainTableConstants.TBL_API_USER_DEVICE)
public class ApiUserDevice extends CommonDomain {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -6523350193361750049L;
	
	public enum OsType {
		IOS,					
		ANDROID
	}
	
	/** <code>name</code> : 장치명 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_NAME, length=50)
	private String name;
	
	/** <code>authToken</code> : 인증토큰(UUID) */
	@Column(name=DomainColumnConstants.TBL_COLUMN_AUTH_TOKEN, length=100, unique=true, nullable=false)
	private String authToken;
	
	/** <code>authToken</code> : 인증토큰 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_AUTH_TOKEN_SNS, length=300, unique=true)
	private String authTokenForSNS;
	
	/** <code>deviceId</code> : 장치 고유 ID */
	@Column(name=DomainColumnConstants.TBL_COLUMN_DEVICE_ID, unique=true, nullable=false, length=200)
	private String deviceId;
	
	/** <code>pushId</code> : pushID */
	@Column(name=DomainColumnConstants.TBL_COLUMN_PUSH_ID, length=300)
	private String pushId;
	
	/** <code>appVersion</code> : 앱 버전 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_APP_VERSION, length=50)
	private String appVersion;
	
	/** <code>loginExpired</code> : 인증 만료 시간 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LOGIN_EXPIRED_DATE)
	private Date loginExpired;
	
	@Column(name=DomainColumnConstants.TBL_COLUMN_OS_TYPE, nullable=false)
	private OsType osType = OsType.IOS;
	
	/** <code>user</code> : 유저 테이블 */
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private ApiUser user;

	/**
	 * @brief name를 전달한다.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @brief name을 설정한다. 
	 *
	 * @param name name 
	 */
	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @brief authToken를 전달한다.
	 *
	 * @return authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @brief authToken을 설정한다. 
	 *
	 * @param authToken authToken 
	 */
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @brief authTokenForSNS를 전달한다.
	 *
	 * @return authTokenForSNS
	 */
	@JsonIgnore
	public String getAuthTokenForSNS() {
		return authTokenForSNS;
	}

	/**
	 * @brief authTokenForSNS을 설정한다. 
	 *
	 * @param authTokenForSNS authTokenForSNS 
	 */
	
	public void setAuthTokenForSNS(String authTokenForSNS) {
		this.authTokenForSNS = authTokenForSNS;
	}

	/**
	 * @brief deviceId를 전달한다.
	 *
	 * @return deviceId
	 */
	@JsonIgnore
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @brief deviceId을 설정한다. 
	 *
	 * @param deviceId deviceId 
	 */
	
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @brief pushId를 전달한다.
	 *
	 * @return pushId
	 */
	public String getPushId() {
		return pushId;
	}

	/**
	 * @brief pushId을 설정한다. 
	 *
	 * @param pushId pushId 
	 */
	
	public void setPushId(String pushId) {
		this.pushId = pushId;
	}

	/**
	 * @brief appVersion를 전달한다.
	 *
	 * @return appVersion
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * @brief appVersion을 설정한다. 
	 *
	 * @param appVersion appVersion 
	 */
	
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * @brief loginExpired를 전달한다.
	 *
	 * @return loginExpired
	 */
	public Date getLoginExpired() {
		return loginExpired;
	}

	/**
	 * @brief loginExpired을 설정한다. 
	 *
	 * @param loginExpired loginExpired 
	 */
	
	public void setLoginExpired(Date loginExpired) {
		this.loginExpired = loginExpired;
	}

	/**
	 * @brief osType를 전달한다.
	 *
	 * @return osType
	 */
	public OsType getOsType() {
		return osType;
	}

	/**
	 * @brief osType을 설정한다. 
	 *
	 * @param osType osType 
	 */
	
	public void setOsType(OsType osType) {
		this.osType = osType;
	}

	/**
	 * @brief user를 전달한다.
	 *
	 * @return user
	 */
	public ApiUser getUser() {
		return user;
	}

	/**
	 * @brief user을 설정한다. 
	 *
	 * @param user user 
	 */
	
	public void setUser(ApiUser user) {
		this.user = user;
	}
	
	

}
