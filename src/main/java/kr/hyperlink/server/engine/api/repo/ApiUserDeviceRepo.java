/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.hyperlink.server.engine.api.domain.ApiUserDevice;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.repo 
 * ApiUserDeviceRepo.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저장치 Repo
 * @description		:	
 * </pre>
 *
 */

public interface ApiUserDeviceRepo extends JpaRepository<ApiUserDevice, Long> {

	public ApiUserDevice findByAuthToken(String xAuthToken);

	public ApiUserDevice findByDeviceId(String deviceId); 

}
