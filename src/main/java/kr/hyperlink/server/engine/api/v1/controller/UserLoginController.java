/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.hyperlink.server.engine.api.model.AppModel;
import kr.hyperlink.server.engine.api.model.User;
import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.api.model.UserResponse;
import kr.hyperlink.server.engine.api.service.ApiUserService;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.controller 
 * UserLoginController.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저 인증 처리 Controller
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@RestController
@CrossOrigin(origins = "*", 
				allowedHeaders = "X-Auth-Token", 
				methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
@RequestMapping(value = "/api/login")
public class UserLoginController {

	@Autowired
	private ApiUserService userService;
	
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public UserApi<UserResponse> userLogin(
			@RequestParam(value="userId", required = true) String userId,
			@RequestParam(value="password", required = false) String password,
			@RequestParam(value="loginType", required = true) String loginType,
			@RequestParam(value="email", required = false) String email,
			@RequestParam(value="authToken", required = false) String authToken,
			@RequestParam(value="pushId", required = false) String pushId,
			@RequestParam(value="deviceId", required = true) String deviceId,
			@RequestParam(value="modelName", required = false) String modelName,
			@RequestParam(value="userName", required = false) String userName,
			@RequestParam(value="osType", required = true) String osType,
			@RequestParam(value="versionInfo", required=true) String versionInfo,
			@RequestPart(value="profileImage", required=false) MultipartFile profileImage
			/*,HttpServletRequest request*/) {
		UserApi<UserResponse> rtn = new UserApi<UserResponse>();

		try {
			rtn = userService.loginUserSNS(userId, password, loginType, email, authToken, pushId, deviceId, modelName, userName, osType, versionInfo, profileImage);
		} catch (ApiControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode()); 
			rtn = new UserApi<UserResponse>(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			rtn = new UserApi<UserResponse>(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR);
		}
		return rtn;
	}
	
	
	@PostMapping(value = "/token")
	public UserApi<?> token(@RequestHeader(value = "X-Auth-Token", required = true) String authToken, 
																				@RequestBody AppModel appModel) {
		UserApi<?> rtn = new UserApi<>();
		try {
			String str = userService.validateUserToken(authToken, appModel);
			rtn.setResStatus(str);
		} catch (ApiControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			rtn = new UserApi<>(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			rtn = new UserApi<>(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR);
		}
		return rtn;
	}
	
	@PostMapping(value = "/etc")
	public UserApi<UserResponse> userLoginEtc(@Validated @RequestBody User user) {
		UserApi<UserResponse> rtn = new UserApi<UserResponse>();
		try {
			rtn = userService.loginUserETC(user);
		} catch (ApiControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			rtn = new UserApi<>(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			rtn = new UserApi<>(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR);
		}
		return rtn;
	}
}
