package kr.hyperlink.server.engine.api.v1.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
@CrossOrigin(origins = "*",
        allowedHeaders = "X-Auth-Token",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
@RequestMapping(value = "/admin/index")
public class IndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String hello() {
        return "index";
    }
}
