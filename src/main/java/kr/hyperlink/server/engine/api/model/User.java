/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import kr.hyperlink.server.engine.api.domain.ApiUser;
import kr.hyperlink.server.engine.api.domain.ApiUserDevice;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.model 
 * User.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저 로그인 모델
 * @description		:	
 * </pre>
 *
 */

public class User  implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 1742313083023692220L;
	
	@NotNull
	@NotEmpty
	/** <code>userId</code> : 유저 아이디 (SNS 일시 SNS ID, 일반 로그인 일시 일반 ID */
	private String userId;
	
	@NotNull
	@NotEmpty
	/** <code>password</code> : 비밀번호 (SNS 일시 빈값) */
	private String password;
	
	/** <code>loginType</code> : 로그인 타입 */
	private ApiUser.LatestLoginType loginType;
	
	/** <code>email</code> : 이메일 */
	private String email;
	
	/** <code>authToken</code> : 인증토큰 (SNS 인증토큰) */
	private String authToken;
	
	/** <code>pushId</code> : 푸시 ID */
	private String pushId;
	
	@NotNull
	@NotEmpty
	/** <code>deviceId</code> : 장치 ID */
	private String deviceId;
	
	/** <code>modelNumber</code> : 장치 모델명 */
	private String modelName;
	
	/** <code>profileImage</code> : 프로필 이미지 (파일) */
	private MultipartFile profileImage;
	
	/** <code>userName</code> : 유저명 */
	private String userName;
	
	@NotNull
	@NotEmpty
	/** <code>versionInfo</code> : 버전 정보 */
	private String versionInfo;
	
	
	/** <code>osType</code> : OS Type */
	private ApiUserDevice.OsType osType;
	
	@NotNull
	@NotEmpty
	private String osTypeParam;

	/**
	 * @brief userId를 전달한다.
	 *
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @brief userId을 설정한다. 
	 *
	 * @param userId userId 
	 */
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @brief password를 전달한다.
	 *
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @brief password을 설정한다. 
	 *
	 * @param password password 
	 */
	
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @brief loginType를 전달한다.
	 *
	 * @return loginType
	 */
	public ApiUser.LatestLoginType getLoginType() {
		return loginType;
	}

	/**
	 * @brief loginType을 설정한다. 
	 *
	 * @param loginType loginType 
	 */
	
	public void setLoginType(ApiUser.LatestLoginType loginType) {
		this.loginType = loginType;
	}

	/**
	 * @brief email를 전달한다.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @brief email을 설정한다. 
	 *
	 * @param email email 
	 */
	
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @brief authToken를 전달한다.
	 *
	 * @return authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @brief authToken을 설정한다. 
	 *
	 * @param authToken authToken 
	 */
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @brief pushId를 전달한다.
	 *
	 * @return pushId
	 */
	public String getPushId() {
		return pushId;
	}

	/**
	 * @brief pushId을 설정한다. 
	 *
	 * @param pushId pushId 
	 */
	
	public void setPushId(String pushId) {
		this.pushId = pushId;
	}

	/**
	 * @brief deviceId를 전달한다.
	 *
	 * @return deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @brief deviceId을 설정한다. 
	 *
	 * @param deviceId deviceId 
	 */
	
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	/**
	 * @brief profileImage를 전달한다.
	 *
	 * @return profileImage
	 */
	public MultipartFile getProfileImage() {
		return profileImage;
	}

	/**
	 * @brief profileImage을 설정한다. 
	 *
	 * @param profileImage profileImage 
	 */
	
	public void setProfileImage(MultipartFile profileImage) {
		this.profileImage = profileImage;
	}

	/**
	 * @brief userName를 전달한다.
	 *
	 * @return userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @brief userName을 설정한다. 
	 *
	 * @param userName userName 
	 */
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @brief osType를 전달한다.
	 *
	 * @return osType
	 */
	public ApiUserDevice.OsType getOsType() {
		return osType;
	}

	/**
	 * @brief osType을 설정한다. 
	 *
	 * @param osType osType 
	 */
	
	public void setOsType(ApiUserDevice.OsType osType) {
		this.osType = osType;
	}

	/**
	 * @brief modelName를 전달한다.
	 *
	 * @return modelName
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * @brief modelName을 설정한다. 
	 *
	 * @param modelName modelName 
	 */
	
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @brief osTypeParam를 전달한다.
	 *
	 * @return osTypeParam
	 */
	public String getOsTypeParam() {
		return osTypeParam;
	}

	/**
	 * @brief osTypeParam을 설정한다. 
	 *
	 * @param osTypeParam osTypeParam 
	 */
	
	public void setOsTypeParam(String osTypeParam) {
		this.osTypeParam = osTypeParam;
	}

	/**
	 * @brief versionInfo를 전달한다.
	 *
	 * @return versionInfo
	 */
	public String getVersionInfo() {
		return versionInfo;
	}

	/**
	 * @brief versionInfo을 설정한다. 
	 *
	 * @param versionInfo versionInfo 
	 */
	
	public void setVersionInfo(String versionInfo) {
		this.versionInfo = versionInfo;
	}
	
	
	

	
}
