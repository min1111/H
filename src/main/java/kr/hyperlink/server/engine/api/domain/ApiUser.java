package kr.hyperlink.server.engine.api.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import kr.hyperlink.server.engine.common.CommonDomain;
import kr.hyperlink.server.engine.constants.DomainColumnConstants;
import kr.hyperlink.server.engine.constants.DomainTableConstants;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.domain 
 * ApiUser.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   단말 유저 도메인
 * @description		:	
 * </pre>
 *
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name=DomainTableConstants.TBL_API_USER)
public class ApiUser  extends CommonDomain implements UserDetails {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -7133035591209349543L;
	
	// 유저권한 
	public enum RoleType {
		ROLE_USER
 	}
	
	// 유저 사용여부
	public enum UserStatus {
		USE, 	
		LOGOUT,
		CHECKOUT,
		FORCED_CHECKOUT,
		RESET_PASSWD
	}
	
	// 최종 로그인 타입
	public enum LatestLoginType {
		FACEBOOK,
		NAVER, 
		KAKAO,
		ETC
	}
	
	//유저 로그인 타입 (테이블 컬럼 제외)
	public enum LoginType {
		REGISTER,			// 최초 생성
		LOGIN				// 로그인 
	}
	
	
	/** <code>name</code> : 유저 이름 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_NAME, length=50)
	private String name;
	
	/** <code>username</code> : 유저 아이디 (일반) */
	@Column(name=DomainColumnConstants.TBL_COLUMN_USER_ID, length=50, unique=true)
	private String username;
	
	/** <code>userIDSNS</code> : 유저 아이디 (페이스북 내부 유저 ID) */
	@Column(name=DomainColumnConstants.TBL_COLUMN_USER_ID_SNS, length=300)
	private String userIDSNS;
	
	/** <code>email</code> : 유저 이메일  (페이스북)*/
	@Column(name=DomainColumnConstants.TBL_COLUMN_EMAIL_SNS, length=100)
	private String emailSNS;
	
	/** <code>email</code> : 유저 이메일(일반) */
	@Column(name=DomainColumnConstants.TBL_COLUMN_EMAIL, length=100, unique=true)
	private String email;
	
	/** <code>profileImagePath</code> : 유저 프로파일 이미지 경로 (URL) */
	@Column(name=DomainColumnConstants.TBL_COLUMN_PROFILE_IMAGE_PATH, length=300)
	private String profileImagePath;
	
	/** <code>roleType</code> : 유저 권한 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_ROLE_TYPE, nullable=false)
	private RoleType roleType;
	
	/** <code>loginType</code> : 최종 로그인 타입 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LOGIN_TYPE, nullable=false)
	private LatestLoginType loginType;
	
	/** <code>status</code> : 유저 상태 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_STATUS, nullable=false)
	private UserStatus status;
	
	/** <code>latestLogin</code> : 유저 마지막 로그인 시간 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LATEST_LOGIN, nullable=false)
	private Date latestLogin;
	
	/** <code>checkoutTime</code> : 회원 탈퇴 시간 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_CHECKOUT_TIME)
	private Date checkoutTime;
	
	/** <code>logoutTime</code> : 로그아웃 시간 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_LOGOUT_TIME)
	private Date logoutTime;
	
	/** <code>password</code> : 유저 비밀번호 */
	@Column(name=DomainColumnConstants.TBL_COLUMN_PASSWORD, length=255)
	private String password;
	
	@Column(name=DomainColumnConstants.TBL_COLUMN_MOBILE_NUMBER, length=100)
	private String mobileNumber;
	
	/** <code>userDevice</code> : 디바이스  */
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<ApiUserDevice> userDevice;
	
	

	/**
	 * @brief name를 전달한다.
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @brief name을 설정한다. 
	 *
	 * @param name name 
	 */
	
	public void setName(String name) {
		this.name = name;
	}



	


	/**
	 * @return the userIDSNS
	 */
	public String getUserIDSNS() {
		return userIDSNS;
	}



	/**
	 * @param userIDSNS the userIDSNS to set
	 */
	public void setUserIDSNS(String userIDSNS) {
		this.userIDSNS = userIDSNS;
	}



	/**
	 * @return the emailSNS
	 */
	public String getEmailSNS() {
		return emailSNS;
	}



	/**
	 * @param emailSNS the emailSNS to set
	 */
	public void setEmailSNS(String emailSNS) {
		this.emailSNS = emailSNS;
	}



	/**
	 * @brief email를 전달한다.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}



	/**
	 * @brief email을 설정한다. 
	 *
	 * @param email email 
	 */
	
	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * @brief profileImagePath를 전달한다.
	 *
	 * @return profileImagePath
	 */
	public String getProfileImagePath() {
		return profileImagePath;
	}



	/**
	 * @brief profileImagePath을 설정한다. 
	 *
	 * @param profileImagePath profileImagePath 
	 */
	
	public void setProfileImagePath(String profileImagePath) {
		this.profileImagePath = profileImagePath;
	}



	/**
	 * @brief roleType를 전달한다.
	 *
	 * @return roleType
	 */
	public RoleType getRoleType() {
		return roleType;
	}



	/**
	 * @brief roleType을 설정한다. 
	 *
	 * @param roleType roleType 
	 */
	
	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}



	/**
	 * @brief loginType를 전달한다.
	 *
	 * @return loginType
	 */
	public LatestLoginType getLoginType() {
		return loginType;
	}



	/**
	 * @brief loginType을 설정한다. 
	 *
	 * @param loginType loginType 
	 */
	
	public void setLoginType(LatestLoginType loginType) {
		this.loginType = loginType;
	}



	/**
	 * @brief status를 전달한다.
	 *
	 * @return status
	 */
	public UserStatus getStatus() {
		return status;
	}



	/**
	 * @brief status을 설정한다. 
	 *
	 * @param status status 
	 */
	
	public void setStatus(UserStatus status) {
		this.status = status;
	}



	/**
	 * @brief latestLogin를 전달한다.
	 *
	 * @return latestLogin
	 */
	public Date getLatestLogin() {
		return latestLogin;
	}



	/**
	 * @brief latestLogin을 설정한다. 
	 *
	 * @param latestLogin latestLogin 
	 */
	
	public void setLatestLogin(Date latestLogin) {
		this.latestLogin = latestLogin;
	}



	/**
	 * @brief checkoutTime를 전달한다.
	 *
	 * @return checkoutTime
	 */
	public Date getCheckoutTime() {
		return checkoutTime;
	}



	/**
	 * @brief checkoutTime을 설정한다. 
	 *
	 * @param checkoutTime checkoutTime 
	 */
	
	public void setCheckoutTime(Date checkoutTime) {
		this.checkoutTime = checkoutTime;
	}



	/**
	 * @brief logoutTime를 전달한다.
	 *
	 * @return logoutTime
	 */
	public Date getLogoutTime() {
		return logoutTime;
	}



	/**
	 * @brief logoutTime을 설정한다. 
	 *
	 * @param logoutTime logoutTime 
	 */
	
	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}



	/**
	 * @brief mobileNumber를 전달한다.
	 *
	 * @return mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}



	/**
	 * @brief mobileNumber을 설정한다. 
	 *
	 * @param mobileNumber mobileNumber 
	 */
	
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	/**
	 * @brief username을 설정한다. 
	 *
	 * @param username username 
	 */
	
	public void setUsername(String username) {
		this.username = username;
	}



	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		RoleType[] roles = RoleType.values();
		int limitRoleType = roleType.ordinal();
		for (int i = 0, l = roles.length; i < l; ++i) {
			RoleType role = roles[i];
			if (i <= limitRoleType) {
				GrantedAuthority auth = new SimpleGrantedAuthority(role.name());
				list.add(auth);
			}
		}
		return list;
	}

	
	
	/**
	 * @brief password을 설정한다. 
	 *
	 * @param password password 
	 */
	
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}

}
