/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.v1.controller;

import kr.hyperlink.server.engine.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kr.hyperlink.server.engine.admin.service.AdminPropertiesService;
import kr.hyperlink.server.engine.api.model.AppModel;
import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.controller 
 * AppController.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   버전체크 등등의 앱 컨트롤러
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@RestController
@CrossOrigin(origins = "*", 
				allowedHeaders = "X-Auth-Token", 
				methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
@RequestMapping(value = "/api/app")
public class AppController {
	
	private UserApi<AppModel> rtn = null;
	
	@Autowired
	private AdminPropertiesService propService;

	@Autowired
	private TestMapper mapper;
	
	@GetMapping(value = "/version/{osType}")
	public UserApi<AppModel> read(@Validated @PathVariable("osType") String osType) { 
		try {
			rtn = propService.findByOsType(osType);
		} catch (ApiControllerException apiEx) {
			log.error("[ An error occueerd due to a code {} ]", apiEx.getCode());
			rtn = new UserApi<AppModel>(apiEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			rtn = new UserApi<AppModel>(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR);
		}
		return rtn;
	}

	@GetMapping
	public UserApi<?> test() {
		List<String> list = mapper.findTest();
		return new UserApi<>();
	}
}
