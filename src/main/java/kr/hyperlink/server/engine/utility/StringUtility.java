/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * <pre>
 * kr.hyperlink.server.engine.utility 
 * StringUtility.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   문자열 처리 유틸
 * @description		:	
 * </pre>
 *
 */

public class StringUtility {

public static final String DELIMITER_PIPLINE = "|";
	
	public static final String DELIMITER_COMMA = ",";
	
	public static final String DELIMITER_WAVE_MARK = "~";
	
	public static final String DELIMITER_PARENTHESES_LEFT = "(";
	
	/**
	 * @brief 처리 결과값 문자열 -> 숫자 변환처리 
	 *
	 * @param rtnStatus 변환할 문자열 HTTP CODE
	 * @return 변환된 HTTP CODE
	 */
	public static HttpStatus convertToStatusCode(String rtnStatus) {
		String str = rtnStatus.replace("R", "");
		if(HttpStatus.values().length > 0) {
			for(HttpStatus httpStatus : HttpStatus.values()) {
				if(httpStatus.toString().equals(str)) {
					return httpStatus;
				}
			}
		}
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}
	
	/**
	 * @brief  문자열을 특정 문자열 기준으로 컷하여 List<String> 타입으로 변환
	 *
	 * @param delimiter 문자열 자르기 기준 문자
	 * @param crop 컷팅 처리 문자열
	 * @param replaceStr 컷팅전 삭제할 문자열 
	 * @return 컷팅된 List<String>
	 */
	public static List<String> cropString (String delimiter, String crop, List<String> replaceStr) {
		List<String> rtn = new ArrayList<>();
		if(replaceStr != null) {
			for(String d : replaceStr) {
				crop = crop.replaceAll(d, "");
			}
		}
		StringTokenizer arrTk = new StringTokenizer(crop, delimiter);
		while(arrTk.hasMoreTokens()) { 
			rtn.add(arrTk.nextToken());
		}
		return rtn;
	}
	
	/**
	 * @brief  로그출력 메소드
	 *
	 * @param obj 출력 객체
	 * @return JSON String
	 */
	public static String prettyPrintLog (Object obj) {
		ObjectMapper om = new ObjectMapper();
		om.enable(SerializationFeature.INDENT_OUTPUT); //pretty print
		try {
			String s = om.writeValueAsString(obj);
			return s;
		} catch (JsonProcessingException e) {
			return "";
		}
	}

	public static String getFileExtention(String originalFilename) {
		int pos = originalFilename.lastIndexOf(".");
		String ext = originalFilename.substring(pos+1);
		return ext;
	}
	
	/**
	 * @brief  HttpServletRequest Header 출력 
	 *
	 * @param req 요청
	 * @param log 로그 생성자
	 */
	public static void headerLog(HttpServletRequest req, Logger log) {
		if (log.isDebugEnabled()) {
			Enumeration<?> e = req.getHeaderNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				log.debug("[Request Header: " + key + " : " + req.getHeader(key) + " ]");
			}
		}
	}
	
	/**
	 * @brief  HttpServletRequest Body 출력 
	 *
	 * @param request 요청
	 * @param log 로그 생성자
	 */
	public static void bodyLog(HttpServletRequest request, Logger log) {
		if (log.isDebugEnabled()) {
			Enumeration<?> enumeration = request.getParameterNames();
			while (enumeration.hasMoreElements()) {
				String name = (String) enumeration.nextElement();
				try {
					if ((String[]) request.getParameterValues(name) != null) {
						log.debug("[Request Body: " + name + " : " + request.getParameter(name) + " ]");
					}
				} catch (Exception e) {
					log.error("[Error to Request MSG + " + e.toString() + " ]");
				}
			}
		}
	}
	
	public enum PasswordType {
		INCLUDE_SP_CHARACTERS,			// 특수문자 포함
		ONLY_AP_UPPER_CASE, 			// 영 대문자만
		ONLY_AP_LOWER_CASE,				// 영 소문자만
		ONLY_DIGIT,						// 숫자만
		LOWER_CASE_OR_DIGIT				// 영 소문자와 숫자
	}
	
	/**
	 * @brief  비밀번호 초기화 처리
	 *
	 * @param type 비밀번호 초기화 타입
	 * @param pwLen 비밀번호 초기화 길이
	 * @return 초기화된 비밀번호
	 */
	public static String randomPassword (PasswordType type, int pwLen) {
		StringBuffer strPwd = new StringBuffer();
		char str[] = new char[1];

		// 특수기호 포함
		if (type == PasswordType.INCLUDE_SP_CHARACTERS) {
			for (int i = 0; i < pwLen; i++) {
				str[0] = (char) ((Math.random() * 94) + 33);
				strPwd.append(str);

			}
		} else if (type == PasswordType.ONLY_AP_UPPER_CASE) {
			for (int i = 0; i < pwLen; i++) {
				str[0] = (char) ((Math.random() * 26) + 65);
				strPwd.append(str);

			}
		} else if (type == PasswordType.ONLY_AP_LOWER_CASE) {
			for (int i = 0; i < pwLen; i++) {
				str[0] = (char) ((Math.random() * 26) + 97);
				strPwd.append(str);

			}
		} else if (type == PasswordType.ONLY_DIGIT) {
			int strs[] = new int[1];
			for (int i = 0; i < pwLen; i++) {
				strs[0] = (int) (Math.random() * 9);
				strPwd.append(strs[0]);
			}
		} else {
			Random rnd = new Random();
			for (int i = 0; i < pwLen; i++) {
				if (rnd.nextBoolean()) {
					strPwd.append((char) ((int) (rnd.nextInt(26)) + 97));
				} else {
					strPwd.append((rnd.nextInt(10)));
				}
			}
		}
		return strPwd.toString();
	}
}
