/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import kr.hyperlink.server.engine.config.ConfigProperties;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

/**
 * <pre>
 * kr.hyperlink.server.engine.config 
 * FileHandler.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   업로드 파일처리 클래스
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@Component
public class FileHandler {
	
	public enum UploadType {
		PROFILE,	// 프로필 이미지
		ETC			// 기타
	}
	
	public static final String WEB_INF_IMAGE_TEMP_PATH = "/WEB-INF/temp/";
	
	public static final String WEB_INF_IMAGE_PATH = "/WEB-INF/images/";
	
	public static final String WEB_INF_USER_PROFILE_PATH = WEB_INF_IMAGE_PATH + "users/";
	
	public static final String WEB_INF_ETC_PATH = WEB_INF_IMAGE_PATH + "etc/";
	
	/** <code>DESTINATION_PATH_PROFILE</code> : 프로파일 경로 */
	public static final String DESTINATION_PATH_PROFILE = "/images/users/";
	
	public static final String DESTINATION_PATH_ETC = "/images/etc/";
	
	public static final String WEB_INF_TEMPLATE_PATH = "/WEB-INF/template/";
	
	private String[] extImg = new String[]{".JPG", ".PNG", ".JPEG", ".GIF"};
	
	public static final String NAV_PATH = "nav/";
	
	@Autowired
	private ConfigProperties configProperties;
	
	/**
	 * @brief  파일 존재 여부 체크
	 *
	 * @param path 파일 경로
	 * @return 파일 존재여부
	 * @throws Exception
	 */
	public boolean existsFile(String path) throws Exception {
		String imagePath = configProperties.getFilePath();
		String realPath = WEB_INF_IMAGE_PATH + path;
		File imgFile = new File(imagePath+realPath);
		if(imgFile.exists()) {
			return true;
		}
		return false;
	}
	
	/**
	 * @brief  존재하는 파일을 byte[] 로 반환하여 전달한다.
	 *
	 * @param path 파일 경로
	 * @return 변환된 파일 byte[]
	 * @throws Exception
	 */
	public byte[] getFile(String path) throws Exception {
		return getFile(path, WEB_INF_IMAGE_PATH);
	}
	
	/**
	 * @brief   존재하는 파일을 byte[] 로 반환하여 전달한다.
	 *
	 * @param path 파일 경로
	 * @return 변환된 파일 byte[]
	 * @throws Exception
	 */
	public byte[] getFileForTemplate(String path) throws Exception {
		return getFile(path, WEB_INF_TEMPLATE_PATH);
	}
	
	private byte[] getFile(String path, String basePath) throws Exception {
		String imagePath = configProperties.getFilePath();
		String realPath = basePath + path;
		InputStream in = new FileInputStream(imagePath+realPath);
		byte[] media = IOUtils.toByteArray(in);
		return media;
	}
	
	/**
	 * @brief  이미지의 확장자를 확인한다.
	 *
	 * @param image 이미지 객체
	 * @return 이미지 확장자 확인여부
	 */
	public boolean isValidImage(MultipartFile image) {
		boolean isValidImage = true;
		if(image == null) {
			return false;
		}
		String imgName = image.getOriginalFilename().toUpperCase();
		if(!(imgName.endsWith(extImg[0]) || imgName.endsWith(extImg[1]) || imgName.endsWith(extImg[2]) || imgName.endsWith(extImg[3]))) {
			return false;
		}
		
		return isValidImage;
	}
	
	/**
	 * @brief  파일 업로드 시 파일 생성
	 *
	 * @param image 이미지 파일 업로드
	 * @return 업로드 이미지 이름
	 * @throws Exception
	 * @throws ApiControllerException
	 */
	public String createPostImage(MultipartFile image, UploadType type) throws Exception, ApiControllerException  {
		String filePath = configProperties.getFilePath();
		String pathTemp = filePath + WEB_INF_IMAGE_TEMP_PATH;
		String pathUpload = null; 
		String destinationPath = null;
		if(type == UploadType.PROFILE) {
			pathUpload = filePath + WEB_INF_USER_PROFILE_PATH;
			destinationPath = DESTINATION_PATH_PROFILE;
		} else {
			pathUpload = filePath + WEB_INF_ETC_PATH;
			destinationPath = DESTINATION_PATH_ETC;
		}
		String uuid = "Img_"+System.currentTimeMillis(); 
		File origin = new File(pathTemp, image.getOriginalFilename());
		if(origin.createNewFile() || origin.exists()) {
			image.transferTo(origin);
		} else {
			log.debug("[ image is not created ]");
		}
		String fileExtention = StringUtility.getFileExtention(image.getOriginalFilename());
		String fileName = new StringBuffer().append(uuid).append(".").append(fileExtention).toString();
		File newFile = new File(pathUpload, fileName);
		if(newFile.createNewFile()) {
			newFile = copyFileUsingFileStreams(origin, newFile);
		} else {
			log.debug("[ image is not created ]");
		}

		return destinationPath+newFile.getName();
	}
	
	
	/**
	 * @brief  파일을 복사한다.
	 *
	 * @param source 복사 대상 파일
	 * @param dest 복사 파일
	 * @throws IOException IO 예외
 	 */
	private File copyFileUsingFileStreams(File source, File dest)throws IOException {
		FileUtils.copyFile(source, dest);
		if(dest.exists()) {
			return dest;
		}
		return null;
	}

	public JSONObject getNavFile(String fileName) throws IOException {
		ClassPathResource resource = new ClassPathResource(NAV_PATH+fileName);
		String path = Paths.get(resource.getURI()).toString();
		InputStream in = new FileInputStream(path);
		String media = IOUtils.toString(in);
		JSONObject json = null;
		try {
			json = new JSONObject(media);
		} catch (JSONException e) {
			json = new JSONObject();
		}
		return json;
	}
}
