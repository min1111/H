/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import java.math.BigInteger;

/**
 * <pre>
 * kr.hyperlink.server.engine.config 
 * IntegerUtility.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   숫자열 처리 유틸클래스
 * @description		:	
 * </pre>
 *
 */

public class IntegerUtility {

	/**
	 * @brief  long 타입을 Integer 타입으로 변환
	 *
	 * @param x long 타입의 자연수
	 * @return x 가 Integer의 값의 최대 숫자보다 높다면 Integer 의 최대숫자를 반환 그렇지 않다면 x를 int 형으로 변환하여 반환
	 */
	public static int convertToInteger (long x) {
		int y = 0;
		if(x > (long) Integer.MAX_VALUE) {
			y = Integer.MAX_VALUE;
		} else {
			y = (int)x;
		}
		return y;
	}	
	
	/**
	 * @brief  Integer Type -> Long Type 변환
	 *
	 * @param x Integer 변수
	 * @return Long 변수
	 */
	public static long convertToLong (int x) {
		long y = (int) x;
		return y;
	}
	
	/**
	 * @brief  String -> Double 형 변환
	 *
	 * @param x String 변수
	 * @return Double 변수
	 */
	public static double convertToDoubleFromString(String x) {
		Double d = Double.parseDouble(x);
		return d;
	}

	public static int getLastPage(long count, int max) {
		int totalPage = ((IntegerUtility.convertToInteger(count)-1)/max)+1;
		return totalPage;
	}

	public static long convertToBigInteger(Object obj) {
		BigInteger i = new BigInteger(String.valueOf(obj));
		return i.longValue();
	}

	public static int convertToIntegerByObject(Object object) {
		return Integer.parseInt(String.valueOf(object));
	}
}
