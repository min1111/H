/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <pre>
 * kr.hyperlink.server.engine.config 
 * DateUtility.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   날짜 처리 유틸리티
 * @description		:	
 * </pre>
 *
 */

public class DateUtility {
	
	public static final String FORMAT_FOR_HOUR = "yyyyMMddkk";
	public static final String FORMAT_FOR_HOUR_FOR_DB = "yyyy-MM-dd:kk";
	public static final String FORMAT_FOR_MINUTES_FOR_DB = "yyyy-MM-dd kk:mm";
	public static final String FORMAT_FOR_MINUTE = "yyyyMMddkkmm";
	public static final String FORMAT_FOR_DAY = "yyyyMMdd";
	public static final String FORMAT_FOR_DAY_WITH_COMMA = "yyyy.MM.dd";
	public static final String FORMAT_FOR_HOUR_WITH_COMMA = "yyyy.MM.dd:kk";
	
	private Calendar calendar = null;
	private SimpleDateFormat dateFormat = null;
	
	public DateUtility () {
		calendar = Calendar.getInstance();
		dateFormat = new SimpleDateFormat(FORMAT_FOR_DAY);
	}
	
	public DateUtility (int month) {
		calendar = Calendar.getInstance();
		dateFormat = new SimpleDateFormat(FORMAT_FOR_DAY);
		this.subMonth(month);
	}
	
	public DateUtility (String format) {
		calendar = Calendar.getInstance();
		dateFormat = new SimpleDateFormat(format);
	}
	
	
	/**
	 * @brief Date format 을 set한다
	 *
	 * @param format set format
	 */
	public void setDateFormat (String format) {
		dateFormat = null;
		dateFormat = new SimpleDateFormat(format);
	}
	
	public void setDate(Date date) {
		calendar.setTime(date);
	}
	
	/**
	 * @brief Month - 1  처리   
	 *
	 * @param month 제거 
	 * @return 세팅된 instance
	 */
	public DateUtility subMonth(int month) {
		if(calendar.get(Calendar.MONTH) == 0){
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
			calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) + 12) - month);
		} else {
			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - month);
		}
		return this;
	}
	
	/**
	 * @brief Month + 1 처리
	 *
	 * @param month 추가
	 * @return 세팅된 instance
	 */
	public DateUtility addMonth(int month){
		calendar.add(Calendar.MONTH, month);
		return this;
	}
	
	/**
	 * @brief  예약 시간을 더한다.
	 *
	 * @param flag 0 ~ 7 (0일경우 30분, 이후 시간 단위)
	 * @return 세팅된 instance
	 */
	public DateUtility addReservedTime (int flag) {
		if(flag == 0) {
			flag = 30;
			calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 1);
			calendar.add(Calendar.MINUTE, flag);
		} else {
			calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 1);
			calendar.add(Calendar.HOUR_OF_DAY, flag);
		}
		return this;
	}
	
	/**
	 * @brief  예약 분을 더한다(분단위). 
	 *
	 * @param mminutes 분단위의 숫자
	 * @return 세팅된 Instance
	 */
	public DateUtility addMinutes(int mminutes) {
		calendar.add(Calendar.MINUTE, mminutes);
		return this;
	}
	
	/**
	 * @brief  예약 시간을 처리한다. 
	 *
	 * @param date 시작일자 (yyyyMMddkkmm)
	 * @return 설정된 시간
	 * @throws ParseException
	 */
	public Date addReservedTimeOfBegin (String date) throws ParseException {
		Date stDate = convertToDate(date, DateUtility.FORMAT_FOR_MINUTE);
		setDate(stDate);
		return getDate();
	}
	
	/**
	 * @brief  일자 빼기 
	 *
	 * @param day 빼기 날짜
	 * @return 세팅된 instance
	 */
	public DateUtility subDay(int day) {
		calendar.add(Calendar.DATE, -day);
		return this;
	}
	
	/**
	 * @brief 일자 더하기
	 *
	 * @param day 더하기 날짜
	 * @return 세팅된 instance
	 */
	public DateUtility addDay(int day) {
		calendar.add(Calendar.DATE, day);
		return this;
	}
	
	/**
	 * @brief 월초로 세팅
	 * @return 세팅된 instance
	 */
	public DateUtility setStartOfTheMonth() {
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return this;
	}
	
	public DateUtility setStartOfTheYear() {
		calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
		return this;
	}
	
	/**
	 * @brief 월말로 세팅
	 * @return 세팅된 instance
	 */
	public DateUtility setEndOfTheMonth() {
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return this;
	}
	
	public DateUtility setEndOfTheYear() {
		calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
		return this;
	}
	
	/**
	 * 
	 * @brief 해당 포멧으로 Date 타입을 변경한다
	 *
	 * @param date 포멧 변경 Date
	 * @return 변경된 Date String
	 */
	public String convertToString(Date date)  {
		return dateFormat.format(date);
	}
	
	/**
	 * @brief 해당 포멧으로 String 타입을 변경한다
	 *
	 * @param date 포멧변경 String
	 * @return 변경된 Date;
	 */
	public Date convertToDate (String date, String formatToString) throws ParseException {
		DateFormat format = new SimpleDateFormat(formatToString);
		Date d = format.parse(date);
		return d;
	}
	
	/**
	 * 
	 * @brief 현재 카렌다 객체를 넘긴다 
	 *
	 * @return calendar instance
	 * @throws NullPointerException
	 */
	public Calendar getCalendar () throws NullPointerException {
		if(calendar == null) {
			throw new NullPointerException();
		}
		return calendar;
	}  
	
	/**
	 * @brief  세팅된 시간을 돌려준다
	 *
	 * @return 세팅된시간
	 * @throws ParseException
	 */
	public Date getDate () throws ParseException {
		String dateStr = dateFormat.format(calendar.getTimeInMillis());
		Date date = dateFormat.parse(dateStr);
		return date;
	}
	
	/**
	 * @brief  세팅된 시간을 문자열로 돌려준다.
	 *
	 * @return 세팅된 시간 문자열
	 * @throws ParseException Date 포멧 문자열 파싱에러
	 */
	public String getDateStr () throws ParseException {
		Date date = this.getDate();
		return this.convertToString(date);
	}
	
	
	/**
	 * @brief  오늘의 자정 23시 59분으로 설정하여 반환
	 *
	 * @return 자정 시간 Today
	 * @throws ParseException
	 */
	public Date getEndOfTodayByMinutes() throws ParseException {
		String str = getDateStr()+"2359";
		Date date = this.convertToDate(str, FORMAT_FOR_MINUTE);
		return date;
	}
	
	/**
	 * @brief 해당월의 일자 시작시간(00시05분) 시간로 설정하여 반환 
	 * 
	 * @return 일 시작시간 Today
	 * @throws ParseException
	 */
	public Date getBeginOfTodayByMinutes() throws ParseException {
		String str = this.getDateStr()+"0005";
		Date date = this.convertToDate(str, FORMAT_FOR_MINUTE);
		return date;
	}

	/**
	 * @brief  해당월을 가져온다
	 *
	 * @return 해당월
	 */
	public int getMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}
	
	/**
	 * @brief  해당 연도의 월을 가져온다.
	 *
	 * @return 해당 연도월
	 */
	public String getMonthCode() {
		int month = calendar.get(Calendar.MONTH) + 1;
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(calendar.get(Calendar.YEAR)));
		
		if(month < 10) {
			buf.append("0"+month);
		} else {
			buf.append(month);
		}
		return buf.toString();
	}
	
	/**
	 * @brief  연단위 문자열을 구한다.
	 *
	 * @return 연단위 문자열
	 */
	public String getYear() {
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(calendar.get(Calendar.YEAR)));
		return buf.toString();
	}
	
	/**
	 * @brief  현재 주차수를 구한다 (연단위)
	 *
	 * @return 해당 일자의 몇 주차 수.
	 */
	public String getWeek() {
		String week;
		int x = calendar.get(Calendar.WEEK_OF_YEAR);
		if(x < 10) {
			week = "0"+String.valueOf(x);
		} else {
			week = String.valueOf(x);
		}
		String weekCode = new StringBuffer().append(String.valueOf(calendar.get(Calendar.YEAR))).append(week).toString();
		return weekCode;
	}
	
	/**
	 * @brief 현재 주차수를 구한다 (연단위) 만약 일요일 이라면 하루를 제외한 주차 수를 구한다. 
	 * 
	 * @return 해당 일자의 몇 주차수
	 */
	public String getWeekWithoutSunday() {
		String week;
		if(isSundayOfToday()) {
			subDay(1);
		}
		int x = calendar.get(Calendar.WEEK_OF_YEAR);
		if(x < 10) {
			week = "0"+String.valueOf(x);
		} else {
			week = String.valueOf(x);
		}
		String weekCode = new StringBuffer().append(String.valueOf(calendar.get(Calendar.YEAR))).append(week).toString();
		return weekCode;
	}
	
	
	/**
	 * @brief  오늘이 일요일 이라면 반환한다.
	 * 
	 * @return 일요일 여부
	 */
	public boolean isSundayOfToday() {
		int num = calendar.get(Calendar.DAY_OF_WEEK); 
		if(num == Calendar.SUNDAY) {
			return true;
		}
		return false;
	}
	
	public boolean isSaturdayOfToday() {
		int num = calendar.get(Calendar.DAY_OF_WEEK); 
		if(num == Calendar.SATURDAY) {
			return true;
		}
		return false;
	}
	
	public boolean isFridayOfToday() {
		int num = calendar.get(Calendar.DAY_OF_WEEK); 
		if(num == Calendar.FRIDAY) {
			return true;
		}
		return false;
	}
	
	/**
	 * @brief  해당 일자가 주말인지 반환한다.
	 *
	 * @return 주말 여부
	 */
	public boolean isWeekend() {
		int num = calendar.get(Calendar.DAY_OF_WEEK); 
		if(num == Calendar.SUNDAY || num == Calendar.SATURDAY || num == Calendar.FRIDAY) {
			return true;
		}
		return false;
	}
		
	public int getCurrentFriday() throws ParseException {
		if(isWeekend()) {
			return -1;
		} else {
			Date now = this.getDate();
			Date fridayDate = this.getCurrentDay(Calendar.FRIDAY).getDate();
			long diff = fridayDate.getTime() - now.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);		
			return IntegerUtility.convertToInteger(diffDays);
		}
	}
	
	public int getToday() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	private DateUtility getCurrentDay (int dateFlag) {
		this.calendar.set(Calendar.DAY_OF_WEEK, dateFlag);
		return this;
	}

}
