/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.constants.HttpConstants;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.utility 
 * HttpUtility.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   Http 처리 헨들러
 * @description		:	
 * </pre>
 *
 */
@Slf4j
public class HttpUtility extends HttpConstants {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 2709933411859622199L;

	public static final String KEY_FOR_X_AUTH_TOKEN = "X-Auth-Token";
	
	private String url;
	
	private HttpURLConnection connection = null;
	
	private OutputStream os = null;
	
	private InputStream is = null;
	
	private InputStreamReader isr = null;
	
	private SSLContext context = null;
	
	private URL urlObj = null; 
	
	//true일경우 https, false http;
	private boolean isHttpType = false;
	
	private int httpStatusCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
	
	/**
	 * @brief Http 처리 생성자 
	 *
	 * @param url 접속할 URL  
	 * @throws ApiControllerException IO 예외
	 */
	public HttpUtility (String url) {
		this.url = url;
		isHttpType = url.startsWith("https") ? true : false;
		log.info("[ Send to Server with URL  {} ]", url);
	}
	
	private String encodeString(Properties params) throws UnsupportedEncodingException {
		StringBuffer sb = new StringBuffer(256);
		Enumeration names = params.propertyNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			String value = params.getProperty(name);
			sb.append(URLEncoder.encode(name, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8") );
			 
			if (names.hasMoreElements()) sb.append("&");
		}
		return sb.toString();
	}	
	
	
	/**
	 * @brief  GET 방식으로 전송한다.
	 *
	 * @param params Map<String, Object> params
	 * @return Response (UserApi<?>)
	 * @throws Exception
	 */
	public UserApi<?> sendToGet(Properties params) throws Exception { 
		UserApi<?> api = new UserApi<>();
		try {
			log.info("[ Send to Server with params  {} ]", params.toString());
			String url = this.url;
			if(params != null) {
				url = new StringBuffer().append(url).append("?").append((encodeString(params))).toString();
			}
			urlObj = new URL(url);
			if(isHttpType) {
				connection = (HttpsURLConnection) urlObj.openConnection();
			} else {
				connection = (HttpURLConnection) urlObj.openConnection();
			}
			
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			
			if(isHttpType) {
				// Set Hostname verification  
				((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {  
				   @Override  
				   public boolean verify(String hostname, SSLSession session) {  
					   // Ignore host name verification. It always returns true.  
					   return true;  
				   }  
				});  
				context = SSLContext.getInstance("TLS");
				
				context.init(null, new TrustManager[] { new X509TrustManager() {

					@Override
					public void checkClientTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub	
					}
					@Override
					public void checkServerTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub
					}
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						// TODO Auto-generated method stub
						return null;
					}  
					
				} }, null);
				
				((HttpsURLConnection) connection).setSSLSocketFactory(context.getSocketFactory()); 
				
			} 
			
			connection.connect();
			
			this.httpStatusCode = connection.getResponseCode();
			
			if (httpStatusCode != HttpURLConnection.HTTP_OK) {
				api.setStatus(httpStatusCode);
				return api;
			}
			
			is = connection.getInputStream();
			isr = new InputStreamReader(is, "UTF-8");

			char[] buffer = new char[1024];
			int readSize;
			StringBuilder sb = new StringBuilder();
			while ((readSize = isr.read(buffer)) != -1) {
				sb.append(buffer, 0, readSize);
			}
			api.addData("json", sb.toString());
			return api;
			
		} catch (Exception e) {
			log.debug("[ An error occueerd due to a {} ]", e.toString());
			api.setStatus(HttpURLConnection.HTTP_INTERNAL_ERROR);
			api.setMessage(e.toString());
			return api;
		}
	}
	
	/**
	 * @brief 페이스북 로그인 인증 처리  
	 * 
	 * @param authCode 페이스북 인증 토큰
	 * @return 인증 결과
	 * @throws Exception
	 */
	public UserApi<?> sendToFacebookWithGet(String authCode) throws Exception {
		Properties params = new Properties();
		params.setProperty(HttpUtility.FACEBOOK_PARAMS_ACCESS_TOKEN, authCode);
		return this.sendToGet(params);
	}
	
	public UserApi<?> sendToNaverOrKakaoWithGet(String authCode) throws Exception { 
		UserApi<?> api = new UserApi<>();
		try {
			log.info("[ Send to Server with params  {} ]", authCode);
			String url = this.url;
			urlObj = new URL(url);
			if(isHttpType) {
				connection = (HttpsURLConnection) urlObj.openConnection();
			} else {
				connection = (HttpURLConnection) urlObj.openConnection();
			}
			
			String Bearer = new StringBuffer().append("Bearer").append(HttpConstants.EMPTY).append(authCode).toString();
			
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", Bearer);
			connection.setUseCaches(false);
			
			if(isHttpType) {
				// Set Hostname verification  
				((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {  
				   @Override  
				   public boolean verify(String hostname, SSLSession session) {  
					   // Ignore host name verification. It always returns true.  
					   return true;  
				   }  
				});  
				context = SSLContext.getInstance("TLS");
				
				context.init(null, new TrustManager[] { new X509TrustManager() {

					@Override
					public void checkClientTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub	
					}
					@Override
					public void checkServerTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub
					}
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						// TODO Auto-generated method stub
						return null;
					}  
					
				} }, null);
				
				((HttpsURLConnection) connection).setSSLSocketFactory(context.getSocketFactory()); 
				
			} 
			
			connection.connect();
			
			this.httpStatusCode = connection.getResponseCode();
			
			if (httpStatusCode != HttpURLConnection.HTTP_OK) {
				api.setStatus(httpStatusCode);
				return api;
			}
			
			is = connection.getInputStream();
			isr = new InputStreamReader(is, "UTF-8");

			char[] buffer = new char[1024];
			int readSize;
			StringBuilder sb = new StringBuilder();
			while ((readSize = isr.read(buffer)) != -1) {
				sb.append(buffer, 0, readSize);
			}
			api.addData("json", sb.toString());
			return api;
			
		} catch (Exception e) {
			log.debug("[ An error occueerd due to a {} ]", e.toString());
			api.setStatus(HttpURLConnection.HTTP_INTERNAL_ERROR);
			api.setMessage(e.toString());
			return api;
		}
	}
	
	/**
	 * @brief  Header 정보에 X-Auth-Token 정보를 처리하기 위한 Handler
	 *
	 * @param headers 처리 헤더
	 * @param json 처리 Body
	 * @return 정보 처리 리턴 값
	 * @throws ApiControllerException
	 */
	public UserApi<?> sendToPostWithHeader(Map<String, String> headers, String json) throws ApiControllerException {
		UserApi<?> api = new UserApi<>();
		try {
			String url = this.url;
			urlObj = new URL(url);
			log.info("[ Send to Server by JSON  {} ]", json);
			if(isHttpType) {
				connection = (HttpsURLConnection) urlObj.openConnection();
			} else {
				connection = (HttpURLConnection) urlObj.openConnection();
			}
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty(KEY_FOR_X_AUTH_TOKEN, headers.get(KEY_FOR_X_AUTH_TOKEN));
			connection.setDoOutput(true);
			if(isHttpType) {
				// Set Hostname verification  
				((HttpsURLConnection) connection).setHostnameVerifier(new HostnameVerifier() {  
				   @Override  
				   public boolean verify(String hostname, SSLSession session) {  
					   // Ignore host name verification. It always returns true.  
					   return true;  
				   }  
				});  
				context = SSLContext.getInstance("TLS");
				
				context.init(null, new TrustManager[] { new X509TrustManager() {

					@Override
					public void checkClientTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub	
					}
					@Override
					public void checkServerTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						// TODO Auto-generated method stub
					}
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						// TODO Auto-generated method stub
						return null;
					}  
					
				} }, null);
				
				((HttpsURLConnection) connection).setSSLSocketFactory(context.getSocketFactory()); 
				
			} 
			
			connection.connect();
			
			os = connection.getOutputStream();
			os.write(json.toString().getBytes("UTF-8"));
			os.flush();
			
			this.httpStatusCode = connection.getResponseCode();
			if (httpStatusCode != HttpURLConnection.HTTP_OK) {
				api.setStatus(httpStatusCode);
				return api;
			}
			
			is = connection.getInputStream();
			isr = new InputStreamReader(is, "UTF-8");

			char[] buffer = new char[1024];
			int readSize;
			StringBuilder sb = new StringBuilder();
			while ((readSize = isr.read(buffer)) != -1) {
				sb.append(buffer, 0, readSize);
			}
			api.addData("json", sb.toString());
			return api;
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			api.setStatus(HttpURLConnection.HTTP_INTERNAL_ERROR);
			api.setMessage(e.toString());
			return api;
		} 
	}

}
