/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import java.util.List;

/**
 * <pre>
 * kr.hyperlink.server.engine.utility 
 * DBUtility.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   DB 특정 기능 처리 클래스
 * @description		:	
 * </pre>
 *
 */


public class DBUtility {
	
	
	/**
	 * @brief  String 타입의  Enum 값을 변환한다. 
	 *
	 * @param enums 변환할 Enum 객체  List
	 * @param value 변환할 문자열
	 * @return 변환된 Enum 객체
	 */
	public static Object convertToEnumByString (List<?> enums, String value) {
		for(Object status : enums) {
			if(status.toString().equals(value)) {
				return status;
			}
		}
		return null;
	}


	/**
	 * @brief  List<T> 객체를 Null로 변환하여 처리
	 *
	 * @param list 변환 list
	 * @return null or list
	 */
	public static List ifNull(List list) {
		if(list != null) {
			if(list.size() == 0) {
				return null;
			}
		}
		return list;
	}
}
