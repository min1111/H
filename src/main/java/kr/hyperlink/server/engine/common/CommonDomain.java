/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * <pre>
 * kr.hyperlink.server.engine.common 
 * CommonDomain.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   최상위 도메인 (With 생성일, 수정일)
 * @description		:	
 * </pre>
 *
 */
@MappedSuperclass
public class CommonDomain implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 2091113891394639780L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "CREATE_DATE", updatable = false)
	private Date createDate;
	
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@PrePersist
	protected void onCreate() {
		createDate = new Date();
	}
	
	@PreUpdate
	public void onUpdate() {
		updateDate = new Date();
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
	
}
