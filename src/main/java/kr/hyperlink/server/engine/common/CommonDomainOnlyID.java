/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * <pre>
 * kr.hyperlink.server.engine.common 
 * CommonDomainOnlyID.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   최상위 도메인 (Only ID)
 * @description		:	
 * </pre>
 *
 */
@MappedSuperclass
public class CommonDomainOnlyID implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -2907295926386574059L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", updatable = false, nullable = false)
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
